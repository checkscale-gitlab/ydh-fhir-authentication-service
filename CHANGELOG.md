# Changelog

All notable changes to this project will be documented in this file.

### [9.0.2](https://github.com/Fdawgs/ydh-fhir-authentication-service/compare/v9.0.1...v9.0.2) (2022-05-19)


### Improvements

* **plugins/jwt-jwks-auth:** skip jwks querying if not jwt token ([f3ed50f](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/f3ed50f1b1fd2fc301c89e66c3fcd864503b71bd))


### Miscellaneous

* **script:** update benchmark script to target test record ([0584ef7](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/0584ef73068fcd696d342f96833099bbde4461ee))


### Continuous Integration

* remove git credentials after checkout ([#636](https://github.com/Fdawgs/ydh-fhir-authentication-service/issues/636)) ([b4a2a65](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/b4a2a65f2affe0c57bf8388f739dbfad888931dd))


### Dependencies

* **deps-dev:** bump eslint-plugin-jsdoc from 39.2.9 to 39.3.0 ([#637](https://github.com/Fdawgs/ydh-fhir-authentication-service/issues/637)) ([bb2acfb](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/bb2acfb4c0ae8281bff9de728837a08556758f49))
* **deps:** bump @fastify/rate-limit from 6.0.0 to 6.0.1 ([#638](https://github.com/Fdawgs/ydh-fhir-authentication-service/issues/638)) ([f6ed209](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/f6ed209834c2c9c5d92ba546c88d900738325715))

### [9.0.1](https://github.com/Fdawgs/ydh-fhir-authentication-service/compare/v9.0.0...v9.0.1) (2022-05-18)


### Miscellaneous

* **bug_report:** use node 18 as placeholder for `node-version` ([faceadb](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/faceadbe165f36ccd6703e571ee4932ce6d4b90a))
* **routes/redirect:** group removed headers; sort alphabetically ([4cb18e8](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/4cb18e8c50e3ab910ea4b15071bfa7c8d3a4ec17))


### Continuous Integration

* **automerge:** fix context ([2ce6bd0](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/2ce6bd0243c1044ff6aff3f9cc68f95811820ed7))
* check `user.login` is dependabot instead of `actor` ([ae6ee0b](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/ae6ee0b5e3e08d76f7d628cb3c8b661138d7160b))
* **ci:** use `lts/*` for node setup in lint job ([2da123a](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/2da123a04a4ff468cbed5566ec2219b324a5551a))
* **ci:** use `node-version` for node matrix key ([aad057a](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/aad057a1bd655316009f60d899c1af0be9e35c10))


### Dependencies

* **deps-dev:** bump @commitlint/cli from 16.2.4 to 17.0.0 ([#628](https://github.com/Fdawgs/ydh-fhir-authentication-service/issues/628)) ([71a55d9](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/71a55d9a0965e2e5d98f0797cb07053b134cab35))
* **deps-dev:** bump @commitlint/config-conventional ([#629](https://github.com/Fdawgs/ydh-fhir-authentication-service/issues/629)) ([5b74cac](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/5b74cac15f77fa6bdb4f4d2d9ead01c25dab256d))
* **deps-dev:** bump eslint-plugin-jest from 26.1.5 to 26.2.2 ([#630](https://github.com/Fdawgs/ydh-fhir-authentication-service/issues/630)) ([315caeb](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/315caeb20062bc57112725dec96be4977237b8e6))
* **deps-dev:** bump glob from 8.0.1 to 8.0.3 ([#631](https://github.com/Fdawgs/ydh-fhir-authentication-service/issues/631)) ([51467a7](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/51467a744d2046bd2fd6ab34c4a5cf1ed709a699))
* **deps:** bump sub-dependencies ([b5e38e4](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/b5e38e4250274ccb6cb7e435d5ef42e6cb4616e8))


### Improvements

* access `fs/promises` api via newer route ([cd65027](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/cd65027a5877d9d3b5aecadd3cedb16c1161e351))
* **routes/redirect:** increase number of undici clients to 128 ([9a478e1](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/9a478e17a686d1ab563a2c20f8b443ec6a54a2ff))
* **routes/redirect:** reduce pipelining to 1 ([f1db550](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/f1db550aefd61cfc611c0fceb2fbc39624f94db7))

## [9.0.0](https://github.com/Fdawgs/ydh-fhir-authentication-service/compare/v8.0.4...v9.0.0) (2022-05-12)


### ⚠ BREAKING CHANGES

* **plugins/jwt-jwks-auth:** `allowedIssuers` key removed from JWT_JWKS_ARRAY env variable. `jwksEndpoint` key renamed to `issuerDomain` in JWT_JWKS_ARRAY env variable. `issuerDomain` key treated as OpenID issuer

### Bug Fixes

* **plugins/jwt-jwks-auth:** log errors from `Promise.any()` ([d29d241](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/d29d2419a2bf19d5533f5abc1d929a47afc25ef1))
* **server:** do not transform 503 http error into 500 http error res ([91c7165](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/91c71654e1021b3acaa2d448f5c6e3a57fc1eb02))


### Improvements

* **plugins/jwt-jwks-auth:** cache jwk for 15 mins ([e8c8349](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/e8c834969515a8fdc05a562a321662208c24702b))
* **plugins/jwt-jwks-auth:** obtain jwks_uri from openid endpoint ([fc5e05b](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/fc5e05bdb6eb06f74abf374dae607547451fc2aa))
* **plugins/jwt-jwks-auth:** remove unused jwt cache ([dbf50c9](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/dbf50c9f67994c24e838f9a4ac7d8c99cf22b402))


### Miscellaneous

* **.eslintrc:** enable `plugin:jest/style` rules ([#620](https://github.com/Fdawgs/ydh-fhir-authentication-service/issues/620)) ([565476e](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/565476e5c51a76bc16f5ed80efd60757548376b8))
* **server:** use optional chaining for error message logging ([6a73d6a](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/6a73d6a009bde5480aed447912fd5a97781458a9))

### [8.0.4](https://github.com/Fdawgs/ydh-fhir-authentication-service/compare/v8.0.3...v8.0.4) (2022-05-11)


### Miscellaneous

* **server:** add missing asterisk to inline comment block ([57a3dea](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/57a3dead3c4fbb86e5a03cc91ff66bac42a3c0ee))


### Improvements

* **routes/redirect:** remove implicit require of `URL` ([53f3a80](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/53f3a805b33feaa9ba57d2e26b78617e703565e7))


### Continuous Integration

* add dependency-review job ([810d6b4](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/810d6b40fc591c9a2569803d67f8fa5b496fdb91))
* **ci:** require `unit-tests` job to pass for `save-pr-number` job to run ([7b47bbc](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/7b47bbcb3201237879c1e89f5f9eaa035d083669))
* **codeql:** only run on pr changes to `.html`, `.js`, and `.yml` files ([c97db57](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/c97db576f01bd7b1c75291d5ebcd63c602116587))
* **codeql:** resolve missing analyses ([e87fc1a](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/e87fc1a04fbd9fa38df4189bcb0d15ef88be09ea))
* **codeql:** specify which files to scan during analysis ([b9598f6](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/b9598f622b5bb771f5f1fcdfcd28ea4b263a8c13))
* **link-check:** replace `npx linkinator` call with github action ([d7d32ee](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/d7d32ee505e19121721d4b027919e51d6807c073))
* only trigger dependency-review on pr ([601f50e](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/601f50e22b2656d34ec43d88166950dcf5d7b159))
* use shorter arg aliases for lockfile lint step ([1c05fa5](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/1c05fa55f6c7dbb9e8d7ff0ac6367297084796bd))
* validate that resolved url matches the package name ([8e88951](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/8e889519d6dd9f1edf833477f3a2b50432a5a840))


### Documentation

* **readme:** add deprecation note ([ed57dc7](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/ed57dc758b3c397f41c621d3ae1c634d2ea147c5))
* **readme:** remove deprecation note ([cd9115e](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/cd9115e7031418c7050e88f18c33376e8d8d5ee6))
* **readme:** remove postman recommendation ([849fff5](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/849fff53f029c8c69213dcc603e926e5f1914a31))
* **readme:** remove snyk badge ([143667c](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/143667c4a85246ebe3597c681aebd4c29bfcd248))
* update deployment steps to use `npm ci` ([6e3af0f](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/6e3af0fc5579ed083f37622eed839f59eb34a80d))


### Dependencies

* **deps-dev:** bump @commitlint/cli from 16.2.3 to 16.2.4 ([#606](https://github.com/Fdawgs/ydh-fhir-authentication-service/issues/606)) ([d668387](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/d668387b88dea1d0b5fa4511b75b29c58cc5d1d8))
* **deps-dev:** bump @commitlint/config-conventional ([#609](https://github.com/Fdawgs/ydh-fhir-authentication-service/issues/609)) ([5c8138e](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/5c8138ee1c85dda441d89b4810387c214ae3250d))
* **deps-dev:** bump @faker-js/faker from 6.1.2 to 6.3.1 ([#614](https://github.com/Fdawgs/ydh-fhir-authentication-service/issues/614)) ([cdb495b](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/cdb495b2ca1a046a543b6335c6eb9cadeca16717))
* **deps-dev:** bump autocannon from 7.8.1 to 7.9.0 ([#603](https://github.com/Fdawgs/ydh-fhir-authentication-service/issues/603)) ([96d612b](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/96d612b5197435190ace91f1dc070aca903c0df7))
* **deps-dev:** bump eslint from 8.13.0 to 8.15.0 ([#598](https://github.com/Fdawgs/ydh-fhir-authentication-service/issues/598)) ([cfbe779](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/cfbe779e0f5d3ee1fb4df4d2e1f0ef8cefee25bf))
* **deps-dev:** bump eslint-plugin-jest from 26.1.4 to 26.1.5 ([#615](https://github.com/Fdawgs/ydh-fhir-authentication-service/issues/615)) ([c54ce35](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/c54ce35e697ecd6f862887aa6de8844b48f5c433))
* **deps-dev:** bump eslint-plugin-jsdoc from 39.1.1 to 39.2.9 ([#611](https://github.com/Fdawgs/ydh-fhir-authentication-service/issues/611)) ([0c147d8](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/0c147d8bf7ec59dfb6570c56f8207cb90e2ec90a))
* **deps-dev:** bump eslint-plugin-security from 1.4.0 to 1.5.0 ([#617](https://github.com/Fdawgs/ydh-fhir-authentication-service/issues/617)) ([29c090b](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/29c090b7106b2b9e9eeba44a8a2ec431bba79f44))
* **deps-dev:** bump husky from 7.0.4 to 8.0.1 ([#596](https://github.com/Fdawgs/ydh-fhir-authentication-service/issues/596)) ([c718d82](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/c718d828290f2c6ef79c38f529ebc58b22242ea5))
* **deps-dev:** bump jest from 27.5.1 to 28.1.0 ([#618](https://github.com/Fdawgs/ydh-fhir-authentication-service/issues/618)) ([e952048](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/e9520487acee37faa79b5bb73cdc258904721fb4))
* **deps-dev:** bump nodemon from 2.0.15 to 2.0.16 ([#616](https://github.com/Fdawgs/ydh-fhir-authentication-service/issues/616)) ([599db93](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/599db938938cf614e5371a2e38220d8b34be3ce0))
* **deps:** bump dotenv from 16.0.0 to 16.0.1 ([#613](https://github.com/Fdawgs/ydh-fhir-authentication-service/issues/613)) ([bea4b23](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/bea4b2373627f7ea77bab51238b87d590dcbbec0))
* **deps:** bump fast-jwt from 1.5.3 to 1.5.4 ([#605](https://github.com/Fdawgs/ydh-fhir-authentication-service/issues/605)) ([360a08d](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/360a08dfd33d4b7af8b343c9dcb36fd5fcaa4e54))
* **deps:** bump fastify from 3.28.0 to 3.29.0 ([#604](https://github.com/Fdawgs/ydh-fhir-authentication-service/issues/604)) ([a74807e](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/a74807ecf237b7cd4eb6d08ae8d9734fd00f4fa7))
* **deps:** bump fastify-bearer-auth from 6.2.0 to 6.3.0 ([#597](https://github.com/Fdawgs/ydh-fhir-authentication-service/issues/597)) ([4ca1861](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/4ca18611c02be2b18b8adcc8c77381c272abf168))
* **deps:** bump fastify-cors from 6.0.3 to 6.1.0 ([#600](https://github.com/Fdawgs/ydh-fhir-authentication-service/issues/600)) ([0742e7b](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/0742e7b64e7d657512cbbb90dd34f0c114b844a8))
* **deps:** bump fastify-reply-from from 6.6.0 to 6.7.0 ([#599](https://github.com/Fdawgs/ydh-fhir-authentication-service/issues/599)) ([2166279](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/21662795b18fb6716b715a18690c59c2dd535da2))
* **deps:** bump github/codeql-action from 1 to 2 ([#595](https://github.com/Fdawgs/ydh-fhir-authentication-service/issues/595)) ([1fdf7c7](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/1fdf7c72612e6efbda3315f72a3688dab756de43))
* **deps:** bump jwks-rsa from 2.0.5 to 2.1.1 ([#612](https://github.com/Fdawgs/ydh-fhir-authentication-service/issues/612)) ([f3b0984](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/f3b0984c9bd4508e09d133b3d61c1af5b15afb09))
* **deps:** bump pino from 7.10.0 to 7.11.0 ([#607](https://github.com/Fdawgs/ydh-fhir-authentication-service/issues/607)) ([81e4727](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/81e4727cec96d5e7a09c6bb2b57a9f90134b045a))
* **deps:** bump sub-dependencies ([7afa685](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/7afa6853b948c3689bd1bf3fedebbe5453e53a15))
* use new `[@fastify](https://github.com/fastify)` org dependencies ([660af8a](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/660af8a94596811977d4c21193d44c98ce109832))

### [8.0.3](https://github.com/Fdawgs/ydh-fhir-authentication-service/compare/v8.0.2...v8.0.3) (2022-04-12)


### Improvements

* **server:** call reply object over raw when overwriting header ([#575](https://github.com/Fdawgs/ydh-fhir-authentication-service/issues/575)) ([0382a89](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/0382a89ea1cbe0b7076f538116bc368062310bd7))


### Continuous Integration

* **automerge:** squash automerge prs ([#578](https://github.com/Fdawgs/ydh-fhir-authentication-service/issues/578)) ([b66b611](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/b66b611d6a2cb5f24d56d7efdec14226fe653a9f))
* **cd:** update org name for release-please-action ([2e9da5a](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/2e9da5a55106eb15cf131f931a9ec7a02be2b9ab))
* reduce workflow permissions to minimum ([ab1f5e5](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/ab1f5e5af535e14f6be07dedeadc45ebed829d6d))
* replace workflow-run-cleanup-action with github concurrency ([cd748e0](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/cd748e0c1127c3f9ff8392fa66bb398b9ab9494e))


### Dependencies

* **deps-dev:** bump @faker-js/faker from 6.1.1 to 6.1.2 ([#584](https://github.com/Fdawgs/ydh-fhir-authentication-service/issues/584)) ([8075268](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/807526820587bbc027d113f3348c5c8ea32c9874))
* **deps-dev:** bump eslint from 8.12.0 to 8.13.0 ([#586](https://github.com/Fdawgs/ydh-fhir-authentication-service/issues/586)) ([c7c42ac](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/c7c42ac60de9e12da1344d53fae1c27fad76afb3))
* **deps-dev:** bump eslint-plugin-import from 2.25.4 to 2.26.0 ([#591](https://github.com/Fdawgs/ydh-fhir-authentication-service/issues/591)) ([11a0454](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/11a0454292758d5325f2e42b235ec9e4eeb8f414))
* **deps-dev:** bump eslint-plugin-jest from 26.1.3 to 26.1.4 ([#587](https://github.com/Fdawgs/ydh-fhir-authentication-service/issues/587)) ([d425a0a](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/d425a0ac78437d2dd57dc669988310a073d23a32))
* **deps-dev:** bump eslint-plugin-jsdoc from 38.1.4 to 39.1.1 ([#585](https://github.com/Fdawgs/ydh-fhir-authentication-service/issues/585)) ([4b3369a](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/4b3369a467596b4310c38dd401407bcc9a984a13))
* **deps-dev:** bump glob from 7.2.0 to 8.0.1 ([#589](https://github.com/Fdawgs/ydh-fhir-authentication-service/issues/589)) ([41a4d63](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/41a4d63b982bb364b67c61d73e5d73ef8a1d079e))
* **deps-dev:** bump prettier from 2.6.1 to 2.6.2 ([#592](https://github.com/Fdawgs/ydh-fhir-authentication-service/issues/592)) ([0a402e1](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/0a402e1e4a350ec28f597fd64c464d8b6927f322))
* **deps:** bump actions/upload-artifact from 2 to 3 ([#583](https://github.com/Fdawgs/ydh-fhir-authentication-service/issues/583)) ([50046c4](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/50046c4b1fa80a283708b8f924e977f9ea3e6b9f))
* **deps:** bump fast-jwt from 1.5.1 to 1.5.3 ([#582](https://github.com/Fdawgs/ydh-fhir-authentication-service/issues/582)) ([d4bf5fe](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/d4bf5fe2d7d1663714772537a24c6555988a30e2))
* **deps:** bump fastify from 3.27.4 to 3.28.0 ([#590](https://github.com/Fdawgs/ydh-fhir-authentication-service/issues/590)) ([5534fca](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/5534fca5f64b8b0f9d235ee8a0108e7de1c3075b))
* **deps:** bump hadolint/hadolint-action from 2.0.0 to 2.1.0 ([c407c17](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/c407c1781f5c73b9d3b54dc0c7c6c8683d4f1780))
* **deps:** bump moment from 2.29.1 to 2.29.2 ([#580](https://github.com/Fdawgs/ydh-fhir-authentication-service/issues/580)) ([402417c](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/402417cda3ef898177bb895b7d6a55e5abf40cac))
* **deps:** bump pino from 7.9.2 to 7.10.0 ([#588](https://github.com/Fdawgs/ydh-fhir-authentication-service/issues/588)) ([add7714](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/add7714ea31c94bc41ffc97d40823e28c0d61adb))
* **deps:** bump pino-pretty from 7.6.0 to 7.6.1 ([#593](https://github.com/Fdawgs/ydh-fhir-authentication-service/issues/593)) ([3af127c](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/3af127cc4338eedfa0d7e2dadb02aab38ac4b49a))
* **docker:** install production deps only ([#581](https://github.com/Fdawgs/ydh-fhir-authentication-service/issues/581)) ([19b5dbb](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/19b5dbb975c29dc7dce4fa4f964e9975dd7ad314))

### [8.0.2](https://github.com/Fdawgs/ydh-fhir-authentication-service/compare/v8.0.1...v8.0.2) (2022-03-30)


### Improvements

* chain response functions ([8361b75](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/8361b755a5fdee3e8b0226b85c60caf2e9655da8))
* **routes:** clean `accept` header conditionals ([0531f9f](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/0531f9f5727a2f044b0cce7594bee4aef5ca9570))


### Dependencies

* **deps-dev:** bump @faker-js/faker from 6.0.0 to 6.1.1 ([836dc14](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/836dc1482a4c04c9eecdd6976b07b14914315571))
* **deps-dev:** bump autocannon from 7.8.0 to 7.8.1 ([1fbacc0](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/1fbacc07cb691ff8d5bfdc9076d5ad62f9de6d05))
* **deps-dev:** bump eslint from 8.11.0 to 8.12.0 ([c3d9b57](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/c3d9b57058f76e3b45230f151b275be9c55445fa))
* **deps-dev:** bump eslint-plugin-jsdoc from 37.9.7 to 38.1.4 ([5d59424](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/5d59424c6655df7d9b38feae3bb48252f6e9709e))
* **deps-dev:** bump prettier from 2.5.1 to 2.6.1 ([4f9faec](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/4f9faec4ccc057f00b4d4f1a35159daae0273ef6))
* **deps:** bump fastify-disablecache from 2.0.6 to 2.0.7 ([5650456](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/56504568fb4beff77975c2aad61dae144f8224f3))
* **deps:** bump fastify-floc-off from 1.0.5 to 1.0.6 ([95dfafa](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/95dfafa62082bec65fb0161b6dad6ba5fc140258))
* **deps:** bump hadolint/hadolint-action from 1.7.0 to 2.0.0 ([995d856](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/995d8568ec91b55b09fb1c404fa985b32bc9a813))
* **deps:** bump pino-pretty from 7.5.4 to 7.6.0 ([e48d6d9](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/e48d6d9fcf9a62947cd75b2576c5baf515f5ebea))

### [8.0.1](https://github.com/Fdawgs/ydh-fhir-authentication-service/compare/v8.0.0...v8.0.1) (2022-03-25)


### Documentation

* improve readability ([8115d46](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/8115d46f6f25a703d4ac22ea1ba3775f1a95a05e))


### Miscellaneous

* **.env.template:** double-quote example strings ([#542](https://github.com/Fdawgs/ydh-fhir-authentication-service/issues/542)) ([c689c13](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/c689c1324baab72a49d301b3d1ddf442b1642b45))
* **plugins/jwt-jwks-auth:** fix invalid jsdoc tag ([f9d7090](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/f9d7090ca611cba0cecf1ae3d7e4a399766c5798))
* **scripts:** remove redundant gitkraken fix from prepare script ([f3f5792](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/f3f5792a6cce4b0a57d0a857fd1f541802cc14bf))
* **scripts:** use shorter arg aliases; remove debugging args from jest ([05df050](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/05df050f4682a1d7ab388bee50dbbbd640345b9b))


### Improvements

* **routes:** add `preValidation` hooks directly into routes ([#540](https://github.com/Fdawgs/ydh-fhir-authentication-service/issues/540)) ([f324793](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/f32479339eb05e9047351472a3eb3cbd51211acd))
* **server:** use new hook config option for rate-limit plugin ([#546](https://github.com/Fdawgs/ydh-fhir-authentication-service/issues/546)) ([d2b905b](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/d2b905b0f01529116f98fd00c4a598a995fe287a))


### Continuous Integration

* add job step names, workflow comments, and whitespace ([c1ff470](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/c1ff470ccec7aeb6b64826187c373483e3cbba0a))
* **codeql-analysis:** remove unused autobuild step ([10a3ab2](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/10a3ab281ea83a0834c91cde9148a911ff321a7b))
* **codeql:** grant minimum permissions to run; rename file ([#549](https://github.com/Fdawgs/ydh-fhir-authentication-service/issues/549)) ([1dd88d3](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/1dd88d3e9ddb473d2624a24ab0146aab3d3eb2c7))
* only save pr number artifact for dependabot ([d17c154](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/d17c154998bea4e7cc566d087756e77e7c7749c0))
* use docker compose v2 ([1583778](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/1583778b748e6e23b1e2cf1fd53029aa46a839a1))


### Dependencies

* **deps-dev:** bump @commitlint/cli from 16.2.1 to 16.2.3 ([c938553](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/c938553851b0aeea5cbacf307e6eb4216c5e9bad))
* **deps-dev:** bump autocannon from 7.7.0 to 7.7.2 ([38f33c4](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/38f33c4fc63251b4bbede5d0f601456b53afd09c))
* **deps-dev:** bump autocannon from 7.7.0 to 7.8.0 ([61c7daf](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/61c7daf666511fa4b3d30447998b447b60d277a3))
* **deps-dev:** bump eslint from 8.10.0 to 8.11.0 ([334a2f3](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/334a2f3d4b03f3b79548489a0f8b3064674d3439))
* **deps-dev:** bump eslint-plugin-jsdoc from 37.9.4 to 37.9.5 ([65ebbbf](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/65ebbbf4cc14e3e1005ca1eec3e7e6a756c710ed))
* **deps-dev:** replace `faker` with `@faker-js/faker` ([#562](https://github.com/Fdawgs/ydh-fhir-authentication-service/issues/562)) ([34e4538](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/34e4538c3c05c8b5174c1f703181b9dd02cc0c2f))
* **deps:** bump actions/checkout from 2 to 3 ([e7e76b5](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/e7e76b5801c1be6b1bb8f904e25fdf2b7f8bf0fb))
* **deps:** bump env-schema from 3.5.2 to 4.0.0 ([f79fc97](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/f79fc9781b386c27aa4e35f58b404dd72d10ab5e))
* **deps:** bump fastify from 3.27.2 to 3.27.4 ([ede06ef](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/ede06efbf25e70909cdc92f48306679554a88806))
* **deps:** bump fastify-reply-from from 6.5.0 to 6.6.0 ([c2a1171](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/c2a1171dbb1acb5413ba7f7d86c7737a11805a5a))
* **deps:** bump fluent-json-schema from 3.0.1 to 3.1.0 ([c7fd344](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/c7fd344d518e1af8a51add8f27aee2afd2292410))
* **deps:** bump hadolint/hadolint-action from 1.6.0 to 1.7.0 ([ac42023](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/ac42023f11348b721ad5a51062b6b911afb1fb32))
* **deps:** bump minimist from 1.2.5 to 1.2.6 ([a36677c](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/a36677cc4d472507d4f03b656064faec79103e2e))
* **deps:** bump node-forge from 1.2.1 to 1.3.0 ([7e64665](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/7e64665cbe959a1314fae3963d154620f8360da5))
* **deps:** bump pino from 7.8.0 to 7.9.2 ([da3c699](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/da3c6994e2a6804286e9c09f9212355db731c290))
* **deps:** bump pino-pretty from 7.5.1 to 7.5.4 ([b42b601](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/b42b601b8150d9e6cc3ba760e0e8e0bc36503fed))
* **deps:** bump sub-dependencies ([#563](https://github.com/Fdawgs/ydh-fhir-authentication-service/issues/563)) ([58a6377](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/58a6377bd7cbfe7dea1b364afef1f1e41607febd))

## [8.0.0](https://github.com/Fdawgs/ydh-fhir-authentication-service/compare/v7.0.3...v8.0.0) (2022-02-28)


### ⚠ BREAKING CHANGES

* Minimum node engine bumped from` >=14.0.0` to `>=16.0.0` to be able to use `Promise.any()`
* **plugins/jwt-jwks-auth:** `JWKS_ENDPOINT`, `JWT_ALLOWED_AUDIENCE`, `JWT_ALLOWED_ISSUERS`, `JWT_ALLOWED_ALGO_ARRAY`, and `JWT_MAX_AGE` env variables removed. Use new `JWT_JWKS_ARRAY` env variable.

### Features

* **plugins/jwt-jwks-auth:** support more than one jwks endpoint ([a245144](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/a245144adea3c739f6de271c58f05f4146c6173a))
* **plugins/jwt-jwks-auth:** support subject claim validation in jwts ([891c605](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/891c6053c4dae84004f32c8b8d03c88d241101f7))


### Bug Fixes

* **config:** renew rate-limit if user attempts req in limit time window ([#509](https://github.com/Fdawgs/ydh-fhir-authentication-service/issues/509)) ([1204b4a](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/1204b4a094dba34efe206529e6fdbf825b444b2f))
* **plugins/jwt-jwks-auth:** throw more meaningful message on error ([5b557b8](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/5b557b8f78d7eda838b06d9a9233190c02bf68d8))


### Miscellaneous

* **.env.template:** remove trailing whitespace ([1beb999](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/1beb99996b9232776cf79d794c26b0f25b306920))
* **.github:** remove trailing whitespace ([482ac75](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/482ac75a0f3865451a7d4763a67eff72bb18cb3d))
* drop support for node 14 ([9b80430](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/9b80430ce5eb6d42fb2d9e1871867bc92b1360c3))
* **plugins/jwt-jwks-auth:** add jsdoc tag for `allowedSubjects` ([807dbbf](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/807dbbfb27a33f25ecf5edfddd4a8f703eedd0fe))
* **routes:** update cors inline comment ([#510](https://github.com/Fdawgs/ydh-fhir-authentication-service/issues/510)) ([ff7a9b4](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/ff7a9b4eac60766fa815d75e4b878476089450a1))


### Improvements

* **config:** call `Error` as constructor, not function ([0bb247a](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/0bb247a6ec3626e4fca515d7d52d45622becc7bc))
* **plugins/jwt-jwks-auth:** explicitly define config values ([#508](https://github.com/Fdawgs/ydh-fhir-authentication-service/issues/508)) ([68010be](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/68010beb56d1bf622f2f5a89c6b6bcffaf284094))


### Dependencies

* **dependabot:** major tags no longer need ignore support ([bbe1861](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/bbe1861c0c1a1ff3e7e3692f33a1c4c420f297a0))
* **deps-dev:** bump @commitlint/cli from 16.1.0 to 16.2.1 ([6415917](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/6415917b284fc5a57ee44a0dbf9e763919ca60f8))
* **deps-dev:** bump @commitlint/config-conventional ([b02fd92](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/b02fd92169144f3f36af0e78075bf3e98427821a))
* **deps-dev:** bump autocannon from 7.6.0 to 7.7.0 ([a901c97](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/a901c97c3c1fc8bc9bc10ef803a03f381688a6bc))
* **deps-dev:** bump eslint from 8.8.0 to 8.9.0 ([4ab1eb6](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/4ab1eb6b45a21972d73c46ed4639ef6005d54826))
* **deps-dev:** bump eslint from 8.9.0 to 8.10.0 ([41c7226](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/41c7226710401383384e86a9b536ece202805b1a))
* **deps-dev:** bump eslint-config-prettier from 8.3.0 to 8.4.0 ([1c44683](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/1c44683eda3f0f64b8acb5418b72852ddb167cb7))
* **deps-dev:** bump eslint-plugin-jest from 26.0.0 to 26.1.1 ([925de49](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/925de49ea4547d44a2e38b678f291712dde1ffc8))
* **deps-dev:** bump eslint-plugin-jsdoc from 37.7.0 to 37.9.4 ([4b1f500](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/4b1f500304de8e6d2a6341de60b36fd2efd7a4ca))
* **deps-dev:** bump jest from 27.4.7 to 27.5.1 ([7b66102](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/7b66102364ff9fbfba4a6c4322105e7d973988e9))
* **deps-dev:** bump mock-jwks from 1.0.1 to 1.0.3 ([a1d8d20](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/a1d8d20685efc2b02bfa5553318dd3f1b858acd3))
* **deps-dev:** bump nock from 13.2.2 to 13.2.4 ([6dedff3](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/6dedff3f174bdf00e27658bd22d71d44f68213e2))
* **deps:** bump actions/github-script from 5 to 6 ([0607e60](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/0607e6090a0b99681d00c003743d3beec8c85daa))
* **deps:** bump actions/setup-node from 2 to 3 ([c6da704](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/c6da704e6eaa5c68b24fda5be8bccf5dd8c255dd))
* **deps:** bump dotenv from 15.0.0 to 16.0.0 ([87625a2](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/87625a2766aca7b0d92aead54f960fc90ec18703))
* **deps:** bump fast-jwt from 1.4.1 to 1.5.1 ([37e16c3](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/37e16c31cbda76c8569bbe13ccfbaef1abd81c3d))
* **deps:** bump fastify from 3.27.0 to 3.27.2 ([12f72b2](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/12f72b226d2f2f5d443174505b8205ab7273cd5e))
* **deps:** bump fastify-autoload from 3.10.0 to 3.11.0 ([8cc5436](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/8cc54362308294f4310508833e92d47aac2ff305))
* **deps:** bump fastify-bearer-auth from 6.1.0 to 6.2.0 ([93a6251](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/93a62512327946b34102877ddee9518ddb6fc31a))
* **deps:** bump fastify-cors from 6.0.2 to 6.0.3 ([d3a71c2](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/d3a71c2529d3732fc4bb0e3d6f8fed0e948e377c))
* **deps:** bump fastify-disablecache from 2.0.5 to 2.0.6 ([b849004](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/b849004ac0ae0c10ba6a00d3bf613a83893f171a))
* **deps:** bump fastify-floc-off from 1.0.4 to 1.0.5 ([ce563c8](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/ce563c8ca0ffc8806e6cf3d96ba95cde4e38112b))
* **deps:** bump fastify-rate-limit from 5.7.0 to 5.7.2 ([1b79be4](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/1b79be4b9697a8a88ca39b1b44ad3df045020431))
* **deps:** bump fastify-reply-from from 6.4.2 to 6.5.0 ([dadf702](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/dadf7022d26e5c49a9944324765c159ea6aaf920))
* **deps:** bump pino from 7.6.5 to 7.8.0 ([56c11df](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/56c11dfba099d80d65a60a48f6626f3bbacc2a11))
* **deps:** bump sub-dependencies ([#537](https://github.com/Fdawgs/ydh-fhir-authentication-service/issues/537)) ([3b50446](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/3b504464a4b59c8cd42807ec0c97bcc3281e56f6))

### [7.0.3](https://github.com/Fdawgs/ydh-fhir-authentication-service/compare/v7.0.2...v7.0.3) (2022-02-01)


### Improvements

* **config:** use boolean schemas ([#477](https://github.com/Fdawgs/ydh-fhir-authentication-service/issues/477)) ([8a48911](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/8a48911e164129b2d764b7fcb706b68e88df5778))
* **server:** reorder plugin registers ([10be53f](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/10be53f1d23cd06cdc9fdaec803f1c8e4c619f9d))


### Dependencies

* **dependabot:** ignore minor and patch commit-lint updates ([#478](https://github.com/Fdawgs/ydh-fhir-authentication-service/issues/478)) ([ec657e5](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/ec657e502ca1f298f927228f99157ded53bf4f6c))
* **dependabot:** use default open-pull-requests-limit value ([71be94f](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/71be94f23629110990720d78240c8892cc712699))
* **deps-dev:** bump @commitlint/cli from 16.0.1 to 16.1.0 ([6f81d99](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/6f81d9950577458153db1845265eb4f0429daec2))
* **deps-dev:** bump autocannon from 7.5.1 to 7.6.0 ([96ea697](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/96ea697e8a0d625928b74d73404930b32cf0cd41))
* **deps-dev:** bump eslint from 8.6.0 to 8.7.0 ([a797c11](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/a797c11921656fd963be45160c84cbde048312b3))
* **deps-dev:** bump eslint from 8.7.0 to 8.8.0 ([99b0e81](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/99b0e815a87f8ecb7f813893925dacbdf637edfd))
* **deps-dev:** bump eslint-plugin-jest from 25.3.4 to 26.0.0 ([806d060](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/806d060628832bbecf6c30d53c4d05b2cb35459e))
* **deps-dev:** bump eslint-plugin-jsdoc from 37.5.1 to 37.7.0 ([cb02281](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/cb02281a4fd39c64e58dc63940c62e844ae68a0c))
* **deps-dev:** bump eslint-plugin-security-node from 1.1.0 to 1.1.1 ([09885a9](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/09885a90605cfb4af9e38cb84a571786b4caacca))
* **deps-dev:** bump nock from 13.2.1 to 13.2.2 ([7b5c87c](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/7b5c87c4ea9c9ac29e594e2f3f5b16c4519f9b35))
* **deps-dev:** pin faker version ([180d8a4](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/180d8a468f4dcc2134cbdc2bd92026b81abc9153))
* **deps:** bump dotenv from 10.0.0 to 14.3.2 ([e6ab6ce](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/e6ab6ce1ed07e8559af9e276460d3187c15a6183))
* **deps:** bump dotenv from 14.3.2 to 15.0.0 ([f627cb8](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/f627cb848e09516822aa2f8e85ba55cd9e29751a))
* **deps:** bump env-schema from 3.5.1 to 3.5.2 ([6344c66](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/6344c666f527bdbabb4f6776771035651e8f76c6))
* **deps:** bump fastify from 3.25.3 to 3.27.0 ([3bab453](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/3bab453d4688a5e12047faa2a7416a62a5603dc6))
* **deps:** bump fastify-autoload from 3.9.0 to 3.10.0 ([d027a6d](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/d027a6dee3d1f8f0fb6e941a9c06e75294dd222f))
* **deps:** bump fastify-disablecache from 2.0.4 to 2.0.5 ([6ee7030](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/6ee70300ec793d8d6d65d0a8041ea32e82afea73))
* **deps:** bump fastify-floc-off from 1.0.3 to 1.0.4 ([883f1eb](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/883f1eb095aa1308218e901300badfb470c7e393))
* **deps:** bump fastify-helmet from 5.3.2 to 7.0.1 ([#493](https://github.com/Fdawgs/ydh-fhir-authentication-service/issues/493)) ([08f779a](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/08f779ada1f39575505516362bf0b80c39751be7))
* **deps:** bump fastify-plugin from 3.0.0 to 3.0.1 ([bebb99f](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/bebb99fd4949f635479e15c6e9f7c6b66ea92c38))
* **deps:** bump fastify-reply-from from 6.4.1 to 6.4.2 ([de13519](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/de135199179bfdc72f2518e339d0cca840406033))
* **deps:** bump file-stream-rotator from 0.5.7 to 0.6.1 ([66a217f](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/66a217f7eeaf9bce32f14f51062becae46ed954d))
* **deps:** bump pino from 7.6.2 to 7.6.4 ([66f2b12](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/66f2b12a572f240f6a1c51ef635d93734e624f16))
* **deps:** bump pino from 7.6.4 to 7.6.5 ([84da7db](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/84da7db32a76a65e2aa867a801ed5f9302367ed5))
* **deps:** bump pino-pretty from 7.3.0 to 7.5.0 ([a6554e7](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/a6554e7d9be7ff1d4e09b5c17bd83bdf81a05db6))
* **deps:** bump pino-pretty from 7.5.0 to 7.5.1 ([fe7e553](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/fe7e553361a56a9189fc1f59b55f56dfdcdd3238))
* **deps:** bump sub-dependencies ([#505](https://github.com/Fdawgs/ydh-fhir-authentication-service/issues/505)) ([548db23](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/548db23336972dad61ddf5fa98996dbad79c66fe))

### [7.0.2](https://github.com/Fdawgs/ydh-fhir-authentication-service/compare/v7.0.1...v7.0.2) (2022-01-06)


### Documentation

* **contributing:** add step for `lint:licenses` script ([47a9d66](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/47a9d66439b0ae1ca4f4532d654c410f02c83faf))
* **contributing:** update husky hook mention ([6cf5d76](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/6cf5d760eee8362501ea348f2601b1e90954920f))


### Continuous Integration

* remove spellcheck workflow ([#466](https://github.com/Fdawgs/ydh-fhir-authentication-service/issues/466)) ([6b25311](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/6b253118a94dfb7c47abb076ec9ada6b3045714f))


### Improvements

* **routes/redirect:** serialize `redirectUrl` ([3d43950](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/3d43950dc62e23b97b9e323c09465d8282cfb07d))


### Miscellaneous

* **config:** add istanbul inline comments ([#469](https://github.com/Fdawgs/ydh-fhir-authentication-service/issues/469)) ([9388cb6](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/9388cb6ffe7226ae815d7f70e29272bdcb7763f6))
* fix `server` jsdoc tag param type ([98e9064](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/98e9064ac77940b259f416ea3a6f802f34824775))
* **scripts:** remove invalid license identifier from `lint:licenses` ([1a72e33](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/1a72e333605329a91a4e2ef6563d8db7b2d1ea52))
* **server:** update inline comment re helmet defaults ([c58c62c](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/c58c62c7e52064ad565ef551a386e98f0663a435))


### Dependencies

* **deps-dev:** bump @commitlint/cli from 15.0.0 to 16.0.1 ([f4d5b10](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/f4d5b108af980800dcea37df8a6eba5eb1d78578))
* **deps-dev:** bump @commitlint/config-conventional ([5e143f8](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/5e143f80a6b32f708f3720bf9c0d91546046f253))
* **deps-dev:** bump autocannon from 7.5.0 to 7.5.1 ([8300360](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/8300360c198d997f40f26f8f63d561d713199ebf))
* **deps-dev:** bump eslint from 8.1.0 to 8.6.0 ([cc7c3e6](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/cc7c3e6ac6a9e48152be27cb57b8bc543e7498cf))
* **deps-dev:** bump eslint-plugin-import from 2.25.3 to 2.25.4 ([8a6bb69](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/8a6bb69ed2d1ed16dc1d62a10bcefa8d1a2f3870))
* **deps-dev:** bump eslint-plugin-jest from 25.3.0 to 25.3.4 ([b468fc0](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/b468fc019650031e9daf12c2dbc3c6dd416cb166))
* **deps-dev:** bump eslint-plugin-jsdoc from 37.4.0 to 37.5.0 ([c851b27](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/c851b27ae6bc68ef20bab7b11c0ecbacfbd8df82))
* **deps-dev:** bump eslint-plugin-jsdoc from 37.5.0 to 37.5.1 ([dcf3470](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/dcf347016740ef9d79de8aff77aee0573aa865ed))
* **deps-dev:** bump jest from 27.4.5 to 27.4.7 ([4c22e38](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/4c22e388fbb4895f5e89f75c14fc8535c039c7cf))
* **deps-dev:** remove lodash ([35cde47](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/35cde473468ff816290b000b5f32bf4f42ec795d))
* **deps:** bump fast-jwt from 1.4.0 to 1.4.1 ([7dc7e51](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/7dc7e51d0a2f629dce6349719b0a880dad86d683))
* **deps:** bump fastify from 3.25.1 to 3.25.3 ([1c5a46d](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/1c5a46d8c6ef74f381c973f4d22fed9ff461520b))
* **deps:** bump fastify-bearer-auth from 6.0.0 to 6.1.0 ([4d373f6](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/4d373f6b91dd2bbb756e6b3f6f0010d3479e1e14))
* **deps:** bump GoogleCloudPlatform/release-please-action from 2 to 3 ([d18551b](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/d18551b82841285d34b8535294c83b1e317bebd4))
* **deps:** bump pino from 7.6.0 to 7.6.2 ([74de64f](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/74de64f17e20e287c181a3ccff74511ff050bc4b))
* **deps:** bump sub-dependencies ([#474](https://github.com/Fdawgs/ydh-fhir-authentication-service/issues/474)) ([c8d1f76](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/c8d1f76e48b0de4e5f6b4be9c7228db1067108f3))

### [7.0.1](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/compare/v7.0.0...v7.0.1) (2021-12-21)


### Miscellaneous

* **.env.template:** clarify jwt env variable comments ([45f0c05](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/45f0c05923ede331914573d5ba92c174ffa17820))
* **server:** update inline comments ([5b306d4](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/5b306d458636cbcd8e59b1fc1543c0e36f37a37a))


### Documentation

* **contributing:** add mention of husky pre-commit hook ([fb4c99a](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/fb4c99a4b758fe56fbf0602e8177f88252f8ac34))


### Dependencies

* **deps-dev:** bump eslint-plugin-jsdoc from 37.2.2 to 37.4.0 ([96d6624](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/96d6624d346dbbcac90e7068286b4cdf5418e763))
* **deps-dev:** bump eslint-plugin-promise from 5.2.0 to 6.0.0 ([0099649](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/0099649a906ba5526048f0f1e31645c8d409faf4))
* **deps-dev:** bump eslint-plugin-security-node from 1.0.14 to 1.1.0 ([b642c43](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/b642c432e791952864da501f1e3bd6b9d755e34a))
* **deps:** bump env-schema from 3.5.0 to 3.5.1 ([e524f44](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/e524f443e497100d365c84bcae5634d1182bc78c))
* **deps:** bump fastify from 3.25.0 to 3.25.1 ([7332eda](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/7332edaa4d497bd70867ce5add6b2536b9ed28b9))
* **deps:** bump pino from 7.5.1 to 7.6.0 ([01e00a8](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/01e00a836e90d92aefcb155d18760953e05820d8))

## [7.0.0](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/compare/v6.2.5...v7.0.0) (2021-12-16)


### ⚠ BREAKING CHANGES

* **config:** `JWT_MAX_AGE` now accepts null or integer values, no longer accepts strings

### Features

* **server:** allow for jwks jwt and bearer token auth to be disabled ([f17290b](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/f17290b2f57c806f633f2569fe94a8b90ca51eb6))


### Bug Fixes

* **config:** `JWT_MAX_AGE` now accepts integers ([06b91ab](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/06b91ab24a2403d083dbee0e15d73c00f8c527ba))


### Documentation

* **coc:** reduce verbosity ([877b436](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/877b43633b416889077dc9404cc2a211b114a961))
* **readme:** fix broken docker link ([4c67d58](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/4c67d582e9b7eff51421ea725723a66f8834c82c))
* **readme:** tidy prerequisite and deployment steps ([2f7d662](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/2f7d662a38fcac1320a04fa5b3190f5d4a5ea9b3))


### Improvements

* **config:** use new `customOptions` in env-schema ([#423](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/issues/423)) ([8b4e795](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/8b4e795a430f53925e727715a29127b0e1b75c45))
* **plugins/jwt-jwks-auth:** replace `jsonwebtoken` with `fast-jwt` ([#434](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/issues/434)) ([79daf9e](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/79daf9e48cd79b34df2a0517675bb255073f344b))
* **routes/redirect:** move auth prehandler hook to parent context ([f7d4567](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/f7d45671418bd2aa9d715c993dec3bc3078fc5e6))
* **routes/redirect:** move auth registering to parent context ([a44ae14](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/a44ae1465501e9f13af9a56b442ec7d63eb9178e))


### Miscellaneous

* **.env.template:** correct example values ([dfe557f](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/dfe557f01c763866d7bf1bf6d1f48b7381bf80bd))
* **husky/pre-commit:** add `lint:licenses` script ([#421](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/issues/421)) ([d238f92](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/d238f925da35bb6130492285520089567e01ea00))
* ignore `.yarnclean` and `yarn.lock` ([#422](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/issues/422)) ([1d3e388](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/1d3e38899e70615e2bd05ee8c5bcc7b654e6d34f))


### Dependencies

* **dependabot:** ignore minor and patch github-actions updates ([#416](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/issues/416)) ([f29b6f7](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/f29b6f7d7af7762d4ca5223fcb6b5af688957301))
* **dependabot:** ignore minor and patch release-please-action updates ([#432](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/issues/432)) ([e93206d](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/e93206d24dcfd4e2aec8c37edccb04d358c7c336))
* **deps-dev:** add nock ([de22d91](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/de22d91917a2252a6a167227aab09ad2c064a2ef))
* **deps-dev:** bump @commitlint/cli from 14.1.0 to 15.0.0 ([2359af2](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/2359af2adc687734cdda66f2d047a65a1a9f9e4f))
* **deps-dev:** bump @commitlint/config-conventional ([2e85b1a](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/2e85b1aef499d9da220e73db0a01a93f40cf170c))
* **deps-dev:** bump eslint-plugin-jest from 25.2.4 to 25.3.0 ([f079e68](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/f079e68f390009cef2ec4fc4388bc233b8593eca))
* **deps-dev:** bump eslint-plugin-jsdoc from 37.0.3 to 37.1.0 ([8af39f6](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/8af39f631517f0b796ada34bdda0bce8609b3c47))
* **deps-dev:** bump eslint-plugin-jsdoc from 37.1.0 to 37.2.2 ([22c555e](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/22c555e12c52e380e3875a4f2259338400aa8e11))
* **deps-dev:** bump eslint-plugin-promise from 5.1.1 to 5.2.0 ([3c0c9eb](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/3c0c9eb215f7fa4894428a96107856ad5a383948))
* **deps-dev:** bump jest from 27.3.1 to 27.4.3 ([685c500](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/685c500bc86fcc24d68b68475f5db040bff4e921))
* **deps-dev:** bump jest from 27.4.3 to 27.4.5 ([3d0d40d](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/3d0d40dff000b14c43c914ea3f1dec9e30f6c047))
* **deps-dev:** bump prettier from 2.4.1 to 2.5.0 ([c7415d9](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/c7415d9b93d11f0126d497cd4ae042b013af86b0))
* **deps-dev:** bump prettier from 2.5.0 to 2.5.1 ([a7dea06](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/a7dea068cd8f1f8977ac5963fc494b344ca52bec))
* **deps:** bump ajv from 8.8.0 to 8.8.1 ([b672542](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/b6725421fc82d91b6342d896f4b751cb61d2c648))
* **deps:** bump fastify from 3.24.0 to 3.24.1 ([5fa63a5](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/5fa63a5f5ee91fb055a214c9e7d7cd328e79566e))
* **deps:** bump fastify from 3.24.1 to 3.25.0 ([bab8aa3](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/bab8aa3b172dc3dd05497630ed0687ff6392dcb4))
* **deps:** bump fastify-rate-limit from 5.6.2 to 5.7.0 ([65d24c9](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/65d24c910dba8b742990a3d4328bab6e9e19c2c1))
* **deps:** bump GoogleCloudPlatform/release-please-action ([2796e2e](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/2796e2ee727825b96de7f8a70608765f6fd66c32))
* **deps:** bump pino from 7.2.0 to 7.5.1 ([fef3764](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/fef37641511bc4eae8799881606a315c4336d056))
* **deps:** bump pino-pretty from 7.2.0 to 7.3.0 ([d3d26a2](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/d3d26a2f426a0a2b4ecb92289dbc6ecfa6352d84))

### [6.2.5](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/compare/v6.2.4...v6.2.5) (2021-11-16)


### Miscellaneous

* **.env.template:** add note regarding required logging variables ([e7fe252](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/e7fe252209ab8baa5d645a90a0095fe488cace28))
* **.prettierrc:** only enable `bracketSameLine` for html ([#388](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/issues/388)) ([6ac94b2](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/6ac94b2da5cc90895df89a38e173f64623944df2))
* **config:** rename `fsp` variable to `fs` ([391cd9e](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/391cd9e7120094230c31d2bf7c002a134215f619))
* stop excess coverage files being generated ([9ee10ed](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/9ee10ed5be74cb4f77690ac6d798e3ead79021c5))


### Documentation

* **readme:** add mention of `docker compose up` ([#408](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/issues/408)) ([a9f995f](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/a9f995f754e0a444f8b522fea2d2386261263ee0))


### Continuous Integration

* **ci:** do not run clean-up on draft prs ([17508bf](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/17508bf9fbf76a072016721f40173fc9bb43f33a))
* **spell-check:** do not run on draft prs ([e89c4d1](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/e89c4d17a1559bcb1bfec1ed40ba896f37a9e68b))
* trigger workflows when drafts marked as "ready to review" ([#409](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/issues/409)) ([632aaa4](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/632aaa4daaf1e023f27561b9550d81e696297972))
* use actions/setup-node's cache option ([#393](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/issues/393)) ([33a526b](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/33a526b2f571bf6df7618a9c78affc66afc8fc0c))


### Dependencies

* **deps-dev:** bump @commitlint/cli from 13.2.1 to 14.1.0 ([4d7de36](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/4d7de363cd8b90bf2dda25fb28d7cdfdf5b4fe91))
* **deps-dev:** bump @commitlint/config-conventional ([21cb2ef](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/21cb2ef059de6c009eed087cbfb31a4307996ebf))
* **deps-dev:** bump eslint from 7.32.0 to 8.1.0 ([8950c04](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/8950c044572bd18006ce6765024c82f7315aa365))
* **deps-dev:** bump eslint-config-airbnb-base from 14.2.1 to 15.0.0 ([0c19ec4](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/0c19ec4096e7d81ae6880577a4ab5846eb8c9d4d))
* **deps-dev:** bump eslint-plugin-import from 2.25.2 to 2.25.3 ([d5702a9](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/d5702a9ef5394ed02fef6804a3db7523b478539f))
* **deps-dev:** bump eslint-plugin-jest from 25.2.2 to 25.2.4 ([497614d](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/497614d7b43f5613fed70173a7f14c108d796e5f))
* **deps-dev:** bump nodemon from 2.0.14 to 2.0.15 ([ff062fb](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/ff062fb41002f5bdd8eae03bf592b0151301d05b))
* **deps:** bump actions/checkout from 2.3.5 to 2.4.0 ([aaa31b0](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/aaa31b094380d1d4a06d189983dce499e506ee43))
* **deps:** bump ajv from 8.6.3 to 8.7.1 ([8267639](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/82676393f88e19fbbe15256f6f5802d888635cb3))
* **deps:** bump ajv from 8.7.1 to 8.8.0 ([339c473](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/339c47351b23affe63be1da31b60ad02d2d55e19))
* **deps:** bump env-schema from 3.4.0 to 3.5.0 ([41b81a0](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/41b81a0f6d65b066a51c353883954113f4e27b77))
* **deps:** bump fastify from 3.22.1 to 3.23.1 ([95cbd74](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/95cbd74e936c9b13f8872178a5d24b5be91ef619))
* **deps:** bump fastify from 3.23.1 to 3.24.0 ([7416de0](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/7416de068b3de6b0127277471cf829e2f19359b3))
* **deps:** bump fastify-sensible from 3.1.1 to 3.1.2 ([b22b785](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/b22b785bd12c4b63fff3e66dcbebfb31d63b6997))
* **deps:** bump pino from 7.0.5 to 7.1.0 ([5d72868](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/5d72868c8e97ae8bb0ad5786bffcbcf1fb3a856a))
* **deps:** bump pino from 7.1.0 to 7.2.0 ([70d6fd7](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/70d6fd7907f2521cbad3b0cef9b666fa145330f0))
* **deps:** bump pino-pretty from 7.1.0 to 7.2.0 ([27bd5f7](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/27bd5f74b2375bc887077429605c5bd50d476ea8))


### Improvements

* **config:** normalize https cert file paths ([5fcd6a3](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/5fcd6a3cc2304d8d76fe05ed4b12091b45ad3744))
* **config:** normalize logging filepath ([5522140](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/5522140d6352391529f65d5c14d3087a04adecee))
* **plugins/jwt-jwks-auth:** handle errors appropriately; remove bearer string ([#406](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/issues/406)) ([1d5652b](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/1d5652b3d0ff4292dee9bc6f58aa51646130b341))
* **routes:** throw `notAcceptable` errors not return ([#396](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/issues/396)) ([e1c365f](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/e1c365fbfe7b980eb65f141e742439798ba33c55))
* **server:** use `path.joinSafe()` over `path.join()` ([#395](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/issues/395)) ([bf6c60a](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/bf6c60abfca8e43a00bb282279b528cb3f549b1a))
* use custom error handler; link errors to requests in logs ([#410](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/issues/410)) ([d64d52d](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/d64d52d7eaed146784326bf738ad8fc33992700d))

### [6.2.4](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/compare/v6.2.3...v6.2.4) (2021-10-29)


### Bug Fixes

* **config:** remove additional env variables ([#367](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/issues/367)) ([d1923c4](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/d1923c425cb9f5c6aff89ae55852a6263bce0073))
* **routes/redirect:** only allow alphabetical chars in `resource` params ([dd35aac](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/dd35aac7f0c4efafbbec593086e260747ead5b9c))


### Documentation

* bump coc from v2.0.0 to v2.1.0 ([#364](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/issues/364)) ([877e1c3](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/877e1c36848af81cf4129217aa13fb1055970692))


### Miscellaneous

* **.eslintrc:** remove redundant `impliedStrict` option ([#362](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/issues/362)) ([acef4d9](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/acef4d91a1f23359d6758fba9e12f08bf5d97db4))
* **routes/redirect:** use raw regex over string for pattern ([e72f719](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/e72f7198b1add965796fb0d529cf99b6c0e3b9d5))
* **routes:** update inline comment re injection attacks ([81f8a32](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/81f8a3246e7a2d03fc8ee4ecb8bf701480550e4d))


### Improvements

* **config:** use secure-json-parse for json parsing ([8bf1b7e](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/8bf1b7e7f274b28dedb6d9c8751b59ad067040e6))
* **routes/redirect:** remove useless escapes ([31eaa30](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/31eaa305a148817389939b4fc4732e91a8bc17cd))


### Dependencies

* **deps-dev:** bump autocannon from 7.4.0 to 7.5.0 ([d955a52](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/d955a527e9ac58e0b4e82287491ee912fb641ce3))
* **deps-dev:** bump eslint-plugin-jest from 25.0.5 to 25.2.2 ([13d728f](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/13d728fd82bf6067247f7a5ad3bbd306d3e0e3b5))
* **deps-dev:** bump eslint-plugin-jsdoc from 36.1.1 to 37.0.3 ([fbd5c0a](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/fbd5c0a90bb2a5297422dba0a3ef34b911198d5d))
* **deps-dev:** bump eslint-plugin-promise from 5.1.0 to 5.1.1 ([654c911](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/654c911a640029ac118b99e237b3a0ff4e59dac5))
* **deps-dev:** bump husky from 7.0.2 to 7.0.4 ([18cd273](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/18cd273bfd7e2e51816aca95fa319fc4862300ad))
* **deps-dev:** bump jest from 27.2.5 to 27.3.1 ([42bbe15](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/42bbe150d7ce585e801ceda5613510f5430ee990))
* **deps-dev:** bump nodemon from 2.0.13 to 2.0.14 ([d9750a3](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/d9750a31c0b0daa6220e03cd7677705d355feee4))
* **deps:** add secure-json-parse ([fbd30f4](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/fbd30f4d00659494f740394e4e9d0b3dd9baa17f))
* **deps:** bump actions/checkout from 2.3.4 to 2.3.5 ([ff1cfd5](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/ff1cfd5e886c50a467675bfd0ef6b8e81c006d50))
* **deps:** bump fastify from 3.22.0 to 3.22.1 ([f123bba](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/f123bbafad13d56bc7c1fe21f1a64b91e64e13f4))
* **deps:** bump fastify-disablecache from 2.0.3 to 2.0.4 ([5cae854](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/5cae8548d63a13af4e9c08f651bdcf265fac14fe))
* **deps:** bump fastify-floc-off from 1.0.2 to 1.0.3 ([ba2ad38](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/ba2ad38af3852684e858cbf898a6e821916e1a86))
* **deps:** bump fastify-reply-from from 6.4.0 to 6.4.1 ([6ba3c0d](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/6ba3c0d34e6df623800badba15511601591d54fa))
* **deps:** bump jwks-rsa from 2.0.4 to 2.0.5 ([249ae0e](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/249ae0e07b5d0aec983da8313b29857700c7628c))
* **deps:** bump pino from 6.13.3 to 7.0.5 ([724e919](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/724e91999f0809cdca25748ce0555d7e145c0eb9))
* **deps:** bump pino-pretty from 7.0.1 to 7.1.0 ([ffc3da2](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/ffc3da2407f9d430f5e357e11e5924d45c98f82a))
* update lockfile from v1 to v2; bump sub-dependencies ([#383](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/issues/383)) ([1a27dcd](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/1a27dcd54f3afffc2938d175262b777e3a0a2c6d))

### [6.2.3](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/compare/v6.2.2...v6.2.3) (2021-10-13)


### Miscellaneous

* **.eslintrc:** remove inaccurate sourcetype ([#351](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/issues/351)) ([877f6ce](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/877f6ce3200ac6a98be0f58af26706748ab3d468))
* **.vscode:** remove deprecated settings ([#352](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/issues/352)) ([1b205e8](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/1b205e8a4766eee197dc99a3ca45695839ad58e9))
* apply eslint rules per line, not file-wide ([71f37e7](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/71f37e736391ccaf3360078089ac182fd800f0c0))
* **server:** update inline comment re clickjacking ([182feed](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/182feedf4e54dd0128a614128297f465bfdfc38d))


### Dependencies

* **deps-dev:** bump @commitlint/cli from 13.2.0 to 13.2.1 ([cec41ef](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/cec41ef85ae19feb100a58878d52169d205c1674))
* **deps-dev:** bump eslint-plugin-import from 2.24.2 to 2.25.2 ([deaa104](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/deaa1043d02968115111711f14705057e50acb86))
* **deps-dev:** bump eslint-plugin-jest from 24.5.0 to 25.0.5 ([f45fc2d](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/f45fc2d55883c0a15b24bb1689ac27886978367f))
* **deps-dev:** bump eslint-plugin-jsdoc from 36.1.0 to 36.1.1 ([41e4c8e](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/41e4c8e0ad7f29a6bbd3b6fdb674ba0ed46f356c))
* **deps-dev:** bump jest from 27.2.4 to 27.2.5 ([d566db0](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/d566db05b7270d25bc6d56512e9a7e8c6ed67343))
* **deps:** bump fastify-reply-from from 6.3.0 to 6.4.0 ([2272d05](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/2272d050e5727cb4e341f2e73045b3900ddffed3))
* **deps:** bump GoogleCloudPlatform/release-please-action ([fed775f](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/fed775f847873f8a62a7e34913cbe5995766e26f))
* **deps:** bump hadolint/hadolint-action from 1.5.0 to 1.6.0 ([ee85f86](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/ee85f86e01a0e2045dfc85e489add30100661d13))
* **deps:** bump under-pressure from 5.7.0 to 5.8.0 ([dcfb0df](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/dcfb0dfa466a6a64b9fe57a40d4d7b83eb71741f))
* **deps:** bump wagoid/commitlint-github-action from 4.1.4 to 4.1.5 ([30af151](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/30af1517d20dba2a403f1ee2bbb4d8b87fd209f8))
* **deps:** bump wagoid/commitlint-github-action from 4.1.5 to 4.1.9 ([92ceba0](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/92ceba0b7062c035ab0cee2a2fb1678652d98c3e))

### [6.2.2](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/compare/v6.2.1...v6.2.2) (2021-10-01)


### Improvements

* **routes:** move cors options route config to config file ([e8a4877](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/e8a4877a42037781b241d3f5ac3f40e6bf59d19d))
* **server:** move helmet config to config file ([faa2689](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/faa2689f78911bf7804111d6a5f8d63adb10012f))
* **server:** reduce globbing use when registering routes ([#333](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/issues/333)) ([28d3f6b](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/28d3f6b09f5f8fb77b5c920309c4d8350aa95d3b))
* **server:** reduce response header size ([933be97](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/933be979d1ffda5d7563ce91c4f31fd39f17f1d0))


### Dependencies

* **deps-dev:** bump @commitlint/cli from 13.1.0 to 13.2.0 ([81f0c0e](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/81f0c0e9085bdff1722db12bce741ebd89275a99))
* **deps-dev:** bump @commitlint/config-conventional ([1792fd1](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/1792fd1955551dfd4bf0d72bf7df208d67279f04))
* **deps-dev:** bump eslint-plugin-jest from 24.4.2 to 24.5.0 ([84baecf](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/84baecfb2b524c2235558f6438e168beb6f6717d))
* **deps-dev:** bump jest from 27.2.1 to 27.2.4 ([e9771e6](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/e9771e634bbd3f19584ea5d7a6262f1705ba14a8))
* **deps:** bump actions/github-script from 4.1 to 5 ([d3c89ac](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/d3c89ac08ea363a0d22fe96bb411dce6642f2a7c))
* **deps:** bump actions/setup-node from 2.4.0 to 2.4.1 ([a47fad7](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/a47fad72255f30149d157dc30542c9c051c6979d))
* **deps:** bump fastify from 3.21.6 to 3.22.0 ([8ca5be8](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/8ca5be8e0f840982701f504fcf66a07b026fb33e))
* **deps:** bump fastify-reply-from from 6.1.0 to 6.3.0 ([913f434](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/913f4346ed4a63041625657ca4b09989428ff41e))
* **docker:** remove package versioning ([a4321ab](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/a4321ab3970973be508ebb20f9c9e046ecb1e1b8))


### Continuous Integration

* **automerge:** update location of octokit rest methods ([7a9e9e6](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/7a9e9e659e9c68f4d6613e4914dfbb15d7a7f3ee))
* ignore hadolint rule DL3018 ([c5cbe26](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/c5cbe2629249c8e613a5436d4838342ae83747a4))
* update hadolint-action namespace ([e9e1f40](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/e9e1f40a8a9ad47e470fae4d16143acc61ea9dfc))


### Miscellaneous

* **.prettierrc:** enable `bracketsameline` option ([#343](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/issues/343)) ([e0dc31a](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/e0dc31adf00fc98d213e224ba6366f145dfa1713))
* tidy inline comments re plugins ([49d25bd](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/49d25bdd25c5ad53f45beb8270a3eb1633d29e4f))

### [6.2.1](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/compare/v6.2.0...v6.2.1) (2021-09-25)


### Documentation

* **readme:** add link to hospital logo ([#315](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/issues/315)) ([74b0e02](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/74b0e02181f15e8002efc9e473801af559f4a249))


### Miscellaneous

* **.env.template:** document `SERVICE_HOST` default ([#313](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/issues/313)) ([1fb16d2](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/1fb16d25386b86728b1a84e1b5ebf6aee98dadfb))
* **server:** update inline comment re child context ([#329](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/issues/329)) ([9010ea3](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/9010ea394403a0782c798f422fd19b1fba6549b6))


### Dependencies

* **deps-dev:** add eslint-plugin-security-node ([#316](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/issues/316)) ([523bf0f](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/523bf0fee94170e595839efe75162dce30432406))
* **deps-dev:** bump eslint-plugin-jest from 24.4.0 to 24.4.2 ([6268c86](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/6268c86facb884f7a9db1ea0d4783a961bda58f3))
* **deps-dev:** bump glob from 7.1.7 to 7.2.0 ([1d2173b](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/1d2173b3bff62ea8654f7a9599d02c1c7bd9eb81))
* **deps-dev:** bump jest from 27.2.0 to 27.2.1 ([9193b49](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/9193b49d67985072f40a8e50ca0cdc43851d217e))
* **deps-dev:** bump nodemon from 2.0.12 to 2.0.13 ([1f98bb5](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/1f98bb5213e02176735d20bfd4c642096a13c9f9))
* **deps-dev:** bump prettier from 2.4.0 to 2.4.1 ([ba0f1fe](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/ba0f1fee5402b15899d47990a1b07a11fc59fc70))
* **deps:** bump fastify from 3.21.1 to 3.21.6 ([49caa34](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/49caa34c9e52089c27882e6f40c5496c8543b4d1))
* **deps:** bump fastify-accepts from 2.0.1 to 2.1.0 ([d172dc6](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/d172dc6bf21346e3e70675982047744877ed24b4))
* **deps:** bump fastify-reply-from from 6.0.1 to 6.1.0 ([45fbc41](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/45fbc417fc541ce1f07b41938abea5319e56f37f))
* **deps:** bump GoogleCloudPlatform/release-please-action ([253ef99](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/253ef99911e77753c52219ef74e345ea5ebb21fb))
* **deps:** bump pino from 6.13.2 to 6.13.3 ([54d4581](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/54d45810d19d3b1d8a537770473d21c84ad8d4df))
* **deps:** bump pino-pretty from 7.0.0 to 7.0.1 ([fa9e8a0](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/fa9e8a0281ccdffbc894bf1b1e4fa0a106bf1e2b))
* **docker:** bump curl from 7.79.0-r0 to 7.79.1-r0 ([#328](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/issues/328)) ([a7d0f77](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/a7d0f7780ee4212ee142b49ab041b27147005342))

## [6.2.0](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/compare/v6.1.0...v6.2.0) (2021-09-15)


### Features

* **config:** support HTTP/2 via `HTTPS_HTTP2_ENABLED` env variable ([#298](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/issues/298)) ([109a902](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/109a902af0edf2771937776fa3cd53ee5ce231ac))


### Bug Fixes

* **config:** check `SERVICE_REDIRECT_URL` is in URI format ([3d11dfc](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/3d11dfcee5710e46be4d8d2f92e373a003d703ca))


### Continuous Integration

* **ci:** revert to workflow-run-clean-action from github concurrency ([028f4eb](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/028f4eb953ab41412c1c1be3944f8f4d8c7977db))


### Improvements

* async/await usage ([3fd13c7](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/3fd13c70a5afc98fbd6071b4ebbe98a9c15d8fc6))


### Miscellaneous

* **.dockerignore:** ignore development documentation ([55fa023](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/55fa0237b4db0cac12940e505a3dbbc831adae71))
* **.husky/.gitignore:** remove now redundant file ([5ab524e](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/5ab524ed3850c294635c4966b8d4e7600e1d2d95))
* **.prettierrc:** override defaults for html, css, and scss files ([#297](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/issues/297)) ([420569d](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/420569d273d5a0f3f59a2c10ac10fe3ab21149ae))
* **.vscode:** add `mhutchie.git-graph` extension ([0d5cb0a](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/0d5cb0adefa4ebde1b58ac1374312d53fcc0af65))
* **package:** update benchmark script ([cb49b6e](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/cb49b6e5ea12157f8b7e56a3c14b090b8af0cd80))


### Dependencies

* **deps-dev:** bump eslint-plugin-jsdoc from 36.0.8 to 36.1.0 ([34de5db](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/34de5db7441269264053ae79e178487ca40a1e0d))
* **deps-dev:** bump jest from 27.1.0 to 27.2.0 ([b3e92ac](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/b3e92acf5b502965588136f8e6ac5da504a95f6b))
* **deps-dev:** bump prettier from 2.3.2 to 2.4.0 ([5b79d73](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/5b79d7345c85f41bee02c91b2180d2b4e08c82de))
* **deps:** bump ajv from 8.6.2 to 8.6.3 ([0469b0c](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/0469b0cc6afd00e50e5d846d81ed504a22dc0c11))
* **deps:** bump env-schema from 3.3.0 to 3.4.0 ([dc18aca](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/dc18aca2de2af3be227b498da1f0616a79843d95))
* **deps:** bump fastify from 3.20.2 to 3.21.1 ([9095ece](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/9095ece21a0a4a3ff02a187eb2fa44bc2af60547))
* **deps:** bump fastify-autoload from 3.8.1 to 3.9.0 ([195c75d](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/195c75d4187caaf6008d8680264e0d6b87e5805e))
* **deps:** bump GoogleCloudPlatform/release-please-action ([a521a1f](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/a521a1fdcf7ad9f882195e06184ab2f3e113336e))
* **deps:** bump pino from 6.13.1 to 6.13.2 ([4531b12](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/4531b1291b0e6d5ce0a44db10e6112db71429520))
* **deps:** bump pino-pretty from 6.0.0 to 7.0.0 ([befa8c4](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/befa8c4cf7e2ecc4b7b754dd39e4322934724988))
* **deps:** bump sub-dependencies ([#312](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/issues/312)) ([baf9d7e](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/baf9d7e401d8631696be2aa1915efab5e1af5819))
* **deps:** bump wagoid/commitlint-github-action from 4.1.1 to 4.1.4 ([0216048](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/0216048140ba7f88c8fcc8e8875d2c191742e713))
* **docker:** bump curl from 7.67.0-r5 to 7.79.0-r0 ([2f69939](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/2f69939dbc57fec0bae2a5847073e4f9d2f8adc5))

## [6.1.0](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/compare/v6.0.1...v6.1.0) (2021-09-06)


### Features

* **config:** add option to set `Access-Control-Max-Age` CORS header ([#290](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/issues/290)) ([ec4b7f7](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/ec4b7f7643b6507aee26a2ed50677c983b68bcca))


### Bug Fixes

* **config:** `SERVICE_REDIRECT_URL` env variable cannot be null ([71e105b](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/71e105b93889992ddb31f1d6d1bc7936f6b24b25))
* **server:** rate limit all 4xx and 5xx responses ([e803082](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/e8030823651a4c09ce3f9355d4ce19fe1068be61))


### Miscellaneous

* **.env.template:** clarify on HTTPS usage ([43afa33](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/43afa33154177ccbdd7530298cd48bf055161304))
* **.env.template:** clarify on required variables ([76df8d6](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/76df8d642ac3d2a1ba1b7e41aa0c9f4f8572b997))
* **.env.template:** remove log level value ([11d4942](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/11d4942b85a48896ad63a7b8daa5cc6412002afd))
* **.github:** use new YAML configured GitHub issue forms ([#292](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/issues/292)) ([90ab659](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/90ab659a3e44f5602e2f9319239f27c3825338fd))


### Continuous Integration

* **ci:** replace workflow-run-cleanup-action with github concurrency ([#293](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/issues/293)) ([13cfe4e](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/13cfe4ee291e901144f5ebcb12b0fb399d91d7df))


### Dependencies

* **deps:** bump fastify-disablecache from 2.0.2 to 2.0.3 ([798d46d](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/798d46dcf241981bcba9a86e6d02c07cf6f14855))
* **deps:** bump fastify-floc-off from 1.0.1 to 1.0.2 ([7286968](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/728696870f3c7f3a945a77664f9ab95b0bef935b))

### [6.0.1](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/compare/v6.0.0...v6.0.1) (2021-09-01)


### Bug Fixes

* **config:** bearer token security scheme format ([#282](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/issues/282)) ([2369280](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/236928005dfe71219fd2cc2609f97319fd4843e3))
* **plugins/jwt-jwks-auth:** stop attempting to rend second res ([cffb9c8](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/cffb9c879baf268b46c3913b261e2e8e39b60455))
* **routes:** rate-limiting not affecting 406 responses ([ff933f7](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/ff933f7a4104badda2404624ae8c581511644ecd))
* **server:** standardise 401 response schema ([9057497](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/90574979713c52a04464cdd67ef3fb7b925de2eb))


### Documentation

* **readme:** add note regarding log retention for nhs digital ([0a9ecf8](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/0a9ecf89100122835f365df952b26f211bfddb2e))


### Improvements

* add clearer summaries and descriptions for route schemas ([db9acd4](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/db9acd49bd9696c0c6b8de47f62cff9e969b2b82))
* **plugins/shared-schemas:** move response schemas to plugin ([9277822](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/9277822fedd579b7e3962c56ecf07f042bc32174))


### Miscellaneous

* **config:** remove excess word in inline comment ([444f99a](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/444f99a35f9d4bc8c4dd6bf6163e277e3083b9dd))


### Dependencies

* **deps-dev:** bump eslint-plugin-import from 2.24.0 to 2.24.2 ([4b673ec](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/4b673ec7d62261b0264e4e0ea3bf79561324adba))
* **deps-dev:** bump eslint-plugin-jsdoc from 36.0.7 to 36.0.8 ([e914b24](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/e914b24ffe92909166cfb0d57b11fa59695085a6))
* **deps-dev:** bump husky from 7.0.1 to 7.0.2 ([2b136d1](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/2b136d172eb8f5c6ee5282763a0200631ddd68b2))
* **deps-dev:** bump jest from 27.0.6 to 27.1.0 ([3a2d1de](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/3a2d1de04f0b9af010415c1f8cece6a989fd0ab5))
* **deps:** bump actions/github-script from 4.0.2 to 4.1 ([c144ebc](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/c144ebcc20b516a94ccc326a94e719767e452084))
* **deps:** bump fastify-autoload from 3.8.0 to 3.8.1 ([0fcc361](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/0fcc3618180d9b6d165d01bf3a8119e012b97347))
* **deps:** bump fastify-rate-limit from 5.6.0 to 5.6.2 ([c2b532d](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/c2b532ddcfcb5e96739529bb353a17998a08efa7))
* **deps:** bump pino from 6.13.0 to 6.13.1 ([fafc8d2](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/fafc8d2b87061e48373b95b7ad60e800e2c6df5b))
* **deps:** bump pino-pretty from 5.1.3 to 6.0.0 ([a147e12](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/a147e12f4d53b91d47510789d6c0af767fc84ae2))

## [6.0.0](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/compare/v5.0.0...v6.0.0) (2021-08-17)


### ⚠ BREAKING CHANGES

* **routes:** `/healthcheck` moved to `/admin/healthcheck`

### Features

* **routes/admin/healthcheck:** add cors header support ([c6ac6a5](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/c6ac6a57c8c841dd0789f0bb43e6411294ce0688))


### Bug Fixes

* **app:** logging grammar fixes ([88b2f73](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/88b2f73a780072c544047bf6234d0a3849a6880a))
* **config:** allow for empty logger env variables ([2698cf7](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/2698cf7fee2882a242b542ef54b1ce0348284770))
* **config:** defaults for undeclared variables ([76e04aa](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/76e04aa3fcc7d536f7f98c4bddb92da3d6cd860e))


### Miscellaneous

* **env:** document default logger values ([6a5a1a4](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/6a5a1a4eae497b11e35717eaba6922445a40bef8))
* **env:** standardise, sort, and group env variables ([41a2d75](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/41a2d75d484b1eee3a2c153f9db6fe318fe2e96d))


### Improvements

* **config:** consolidate logger pretty print conditional ([384049a](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/384049aad16f2298c202f56864d62df7d917bc84))
* replace `http-errors` with `fastify-sensible` plugin ([9890a07](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/9890a078ddef26900fdf2d4a9c8e5d45cfe5b5ea))
* **routes:** `/healthcheck` moved to `/admin/healthcheck` ([990428e](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/990428e0e8d91fe8858d90062b5bdbe5d9e0b847))


### Dependencies

* **deps-dev:** bump eslint-plugin-import from 2.23.4 to 2.24.0 ([24eb39d](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/24eb39d088dcea04aa391cdc6e6bcfe736c41bdc))
* **deps-dev:** bump eslint-plugin-jsdoc from 36.0.6 to 36.0.7 ([78d1da4](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/78d1da4d764f85f181e1038799ca44ab78d79c77))
* **deps:** bump actions/setup-node from 2.3.0 to 2.4.0 ([1b0b04f](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/1b0b04f0e0a15b2eef6af1183fb4c46348363957))
* **deps:** bump ajv-formats from 2.1.0 to 2.1.1 ([a4f360a](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/a4f360ab1de068a09dd882b94b213117d84b1e7b))
* **deps:** bump env-schema from 3.1.0 to 3.3.0 ([1971244](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/1971244e6bc5b75d6a5dc80fe8bccfd8db1120cf))
* **deps:** bump fastify from 3.19.2 to 3.20.2 ([3c0721c](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/3c0721cd4cc4a9f5382830f57cf6a665808c958a))
* **deps:** bump pino-pretty from 5.1.2 to 5.1.3 ([76942cc](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/76942cc34d0c357c010bd49a50e45e3b7ad6f85b))

## [5.0.0](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/compare/v4.0.3...v5.0.0) (2021-08-02)


### ⚠ BREAKING CHANGES

* minimum required version of node increased from 12 to 14 to allow for new ECMAScript syntax to be used

### Bug Fixes

* **docker-compose:** wrap variables in quotes ([#242](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/issues/242)) ([c83f3b9](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/c83f3b9ffc09899a1232e02e9194b3d4e29f4b6b))


### Improvements

* **config:** provide custom ajv instance to `env-schema` ([#240](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/issues/240)) ([c91c8cd](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/c91c8cd30cc18a6533ca4677ffcb238fcc4ae95d))


### Continuous Integration

* **ci:** remove redundant env variable ([2e3ec3e](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/2e3ec3e5a90df2e399c7988d0127f01e1a2d97e3))


### Dependencies

* **deps-dev:** bump eslint from 7.31.0 to 7.32.0 ([bfd632b](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/bfd632ba30e006f643f1d3e842ec0c9d4943732c))
* **deps-dev:** remove unused fastify-formbody dependency ([5686c2d](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/5686c2d746fbd98494d60162a80bfa90b573e013))
* **deps:** bump actions/setup-node from 2.2.0 to 2.3.0 ([c0c89ac](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/c0c89ac56eb12d2e717650ba4cf3519df3d2be32))
* **deps:** bump dependencies ([a28a4a4](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/a28a4a499d800909fd5829863b6f2dc1813978d1))
* **deps:** bump GoogleCloudPlatform/release-please-action ([ccd8935](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/ccd89350792e281554d555e4161fba3792456fb3))
* **docker:** bump curl from 7.67.0-r4 to 7.67.0-r5 ([34f0107](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/34f01078a9cb07af3179a9e99ec49ab89b6eb836))


### Miscellaneous

* grammar fixes for jsdoc tags ([#256](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/issues/256)) ([f3fef78](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/f3fef78854c027e3e3c9ece979c9fadf7cca9707))
* increase minimum required version of node from 12 to 14 ([#258](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/issues/258)) ([8a81193](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/8a81193fb816395c15665b04055cf236a5b46875))

### [4.0.3](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/compare/v4.0.2...v4.0.3) (2021-07-19)


### Bug Fixes

* **package:** move `pino-pretty` to production dependency list ([#230](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/issues/230)) ([3fb4011](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/3fb4011ab1ae05c4fb1ae4f8a1d3ec0cddb4acde))


### Improvements

* **routes/healthcheck:** move `Accept` header handling back to hook ([1dcd355](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/1dcd3554459915595cf275ee871c3fcb8600cc3a))
* **routes/redirect:** move `Accept` header handling back into hook ([55eded1](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/55eded19b5542ef9f6d0ca83c0f929c560a27815))
* **routes:** do not treat routes as plugins ([b883b1f](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/b883b1fd5e8866e88ad636cf0deafb445d98e14c))
* **server:** move redirect route and auth plugins into new context ([7e5a07e](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/7e5a07e123e23bf23637f396846e185264468d87))


### Continuous Integration

* **cd:** move perf optimizations and refactoring into same section ([97fe2ae](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/97fe2ae082816a5b346fb29d59092c7c4b266e45))


### Dependencies

* **deps-dev:** bump eslint from 7.30.0 to 7.31.0 ([59f0d3a](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/59f0d3a4877d01d3d388ebd6b519d34a14beefd2))
* **deps-dev:** bump eslint-plugin-jsdoc from 35.4.3 to 35.4.5 ([85463a1](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/85463a1679bcd6a5f3443a2a932b19eede8d61d3))
* **deps:** bump fastify from 3.19.0 to 3.19.1 ([92faeb6](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/92faeb6cd6feca3208e87501730cb3270375baa1))
* **deps:** bump fastify-cors from 6.0.1 to 6.0.2 ([ef16081](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/ef1608102db54e365cf72d1803a42c5d61a6b89b))
* **deps:** bump jwks-rsa from 2.0.3 to 2.0.4 ([cb2b03d](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/cb2b03df24ad54b51709f81de95d91c5bfeaf0c7))
* **deps:** bump wagoid/commitlint-github-action from 3.1.4 to 4.1.1 ([516aef2](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/516aef233af2d1222a305e744a23c94100e8b025))
* **dockerignore:** add dev files ([5203e92](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/5203e924a0d933e6612cbc6ddbe4f728db1d5ebc))
* **docker:** use native logging, healthcheck, restart and res handling ([ae00eb4](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/ae00eb4eddcaa36a0d48542a76f0d91c0d21f76e))


### Miscellaneous

* change mentions of "MIME type" to "media type" ([#225](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/issues/225)) ([adda5e9](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/adda5e9c17dbb188861907f1cd61a1f4a4368d72))
* **env.template:** use double quotes ([678dc45](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/678dc45e7195281947495f3487464d7dd2b7828d))
* **server:** sort plugin registering alphabetically ascending ([0551c8e](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/0551c8e8a24f06a438a73cc349b6a4f93175e25d))
* **server:** update encapsulation comment ([92ed426](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/92ed4263bd2d47df8abdf79b8992463b3cc69eb9))
* **test_resources:** fix name of test requests file ([aa3fffe](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/aa3fffe8af83ba7b55435632a288908a7c2b97ca))
* **test_resources:** update test requests with new headers ([873624a](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/873624a949765600bc4aa0b09827ee58fa5060f9))
* update jsdoc tag comments ([4e45184](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/4e451845c61c6acd237c93d56cd4207969bd15a9))
* update plugin metadata for server dependency graph ([288728c](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/288728c3f9bae19751994f6d32b4fd85e74db724))

### [4.0.2](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/compare/v4.0.1...v4.0.2) (2021-07-12)


### Bug Fixes

* **routes:** `Accept` header handling encapsulation ([#217](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/issues/217)) ([e0234d3](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/e0234d3abf44beab3df4040b08940b53b41dc2bf))


### Miscellaneous

* **vscode:** remove user space config setting ([b4c398c](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/b4c398c835ead58ae157f6dacd7e5b95886b6991))


### Dependencies

* **deps-dev:** bump eslint-plugin-jsdoc from 35.4.2 to 35.4.3 ([34d24f1](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/34d24f13ec86e5e6927bf07cf2faf4e54acf9486))
* **deps-dev:** bump nodemon from 2.0.10 to 2.0.12 ([fe8a2b7](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/fe8a2b73925c1402699f760f3ac609dc8a0b7a17))
* **deps:** bump fluent-json-schema from 3.0.0 to 3.0.1 ([bb86510](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/bb8651074890a5183465e9576563c266b8e19850))
* **deps:** bump pino from 6.11.3 to 6.12.0 ([7a39729](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/7a397298187187a9a9ab7186a80a82f754c9b80d))

### [4.0.1](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/compare/v4.0.0...v4.0.1) (2021-07-09)


### Bug Fixes

* **routes/healthcheck:** add `Accept` request header handling ([abe17d5](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/abe17d5d0c80103a777893179111498909dd3d5a))


### Miscellaneous

* **vscode:** disable redhat telemetry ([200c890](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/200c890a0609eb66cf28f60563db2629d219ca1a))


### Dependencies

* **deps-dev:** bump autocannon from 7.3.0 to 7.4.0 ([5166b77](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/5166b77f2692d5e4b7283ee42fe673b2f17bf9d0))
* **deps-dev:** bump coveralls from 3.1.0 to 3.1.1 ([21e5933](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/21e593387100a7f1b84dddfb4c77b6eb46f846b2))
* **deps-dev:** bump eslint from 7.29.0 to 7.30.0 ([c717314](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/c71731465961cd605ab9bb172c9b51f9e92a38a2))
* **deps-dev:** bump eslint-plugin-jsdoc from 35.4.0 to 35.4.1 ([49cb77b](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/49cb77bb2810a4ac946ed62a3468024f910d3b75))
* **deps-dev:** bump eslint-plugin-jsdoc from 35.4.1 to 35.4.2 ([975f8be](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/975f8be231593e1131e485c83c31aaad3058a8a2))
* **deps-dev:** bump husky from 6.0.0 to 7.0.0 ([0e76d7f](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/0e76d7ffab0b76bb91555a5f2cb4c07a8dbbfb5b))
* **deps-dev:** bump husky from 7.0.0 to 7.0.1 ([9603f47](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/9603f475a28bd40b8c001f82161bf923840da176))
* **deps-dev:** bump jest from 27.0.5 to 27.0.6 ([74c1327](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/74c1327f37e68aacc5f90de7cb977fa26c942fdd))
* **deps-dev:** bump nodemon from 2.0.7 to 2.0.9 ([69f076d](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/69f076dc2590eb14f152efbe7bfc95ff0d4f9888))
* **deps-dev:** bump nodemon from 2.0.9 to 2.0.10 ([234b68e](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/234b68edcd3a389fc263ff9ac9f1434fcaabbffa))
* **deps-dev:** bump pino-pretty from 5.0.2 to 5.1.0 ([49ce719](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/49ce71974c89ccddbfc7a224a0fec327e956b41e))
* **deps-dev:** bump pino-pretty from 5.1.0 to 5.1.1 ([f4f7f17](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/f4f7f17057b8f3f22c193adfd941a343f37e19ac))
* **deps-dev:** bump prettier from 2.3.1 to 2.3.2 ([846b0f7](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/846b0f7b5db473dc74a6f40053706f7b2a9592cb))
* **deps:** bump actions/setup-node from 2.1.5 to 2.2.0 ([eb5ae9a](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/eb5ae9ab6e36efe577436109449da38edfb4a98d))
* **deps:** bump coverallsapp/github-action from 1.1.2 to 1.1.3 ([14afe16](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/14afe16b30dc431b0dcbec0ac7548c22208ed1f3))
* **deps:** bump fastify from 3.18.0 to 3.18.1 ([119f704](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/119f704020bd40364f3d81321050a9fd1a84444e))
* **deps:** bump fastify from 3.18.1 to 3.19.0 ([afadc74](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/afadc740c835e6cc4114685a71bcb2e90955f49e))
* **deps:** bump fastify-helmet from 5.3.1 to 5.3.2 ([3ae6470](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/3ae6470f9910c454777eeb3f60df51d269b0074a))
* **deps:** bump fastify-reply-from from 5.3.0 to 6.0.1 ([13f6881](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/13f6881647ca9f8fc625c5ad59520e91f9f81140))

## [4.0.0](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/compare/v3.0.5...v4.0.0) (2021-06-22)


### ⚠ BREAKING CHANGES

* **routes/redirect:** Service no longer falls back to "*" wildcard for `access-control-allow-origin` if `CORS_ORIGIN` env variable is set to true to reflect request "Origin" but the request "Origin" header is missing. Now it will not set the header at all.
* **routes/redirect:** Service no longer falls back to Mirth Connect's `access-control-allow-origin` if `CORS_ORIGIN` env variable is not set.

### Bug Fixes

* **routes/redirect:** do not use "*" if req origin header missing ([c16624b](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/c16624b3fa9d961ad6b24dca3f5700afa05bcc9d))
* **routes/redirect:** do not use mirth's `access-control-allow-origin` ([49bce7f](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/49bce7fb61c23fe2309a17c3d377c892f2e5aeb4))
* **server:** increase `Strict-Transport-Security` max age to 365 days ([d2db435](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/d2db4359c3a0aa34d7e3172f939ff20d00463e6d))
* **server:** revert `Referrer-Policy` directives to "no-referrer" only ([c20312f](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/c20312f421a484e4b15ccc07065c36ce32aff418))
* **server:** use stricter `Content-Security-Policy` values ([93dd790](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/93dd790372889f201928b6985797c18ae27e2ca2))


### Continuous Integration

* **link-check:** reduce frequency from weekly to monthly ([#185](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/issues/185)) ([81959d0](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/81959d0d92340f7ea9ad6384c24fb23c73257d08))


### Miscellaneous

* **server:** clarify on what each registered plugin does ([54f950e](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/54f950eb4035b277dd08917594c1b4cd63573216))


### Dependencies

* **deps-dev:** bump eslint from 7.28.0 to 7.29.0 ([d453e40](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/d453e402f16d23ced0f60ecd7cceabc469520686))
* **deps-dev:** bump eslint-plugin-jsdoc from 35.3.0 to 35.4.0 ([d2ab988](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/d2ab9881304870846799c6eaf920512da0c6c5b4))
* **deps-dev:** bump jest from 27.0.4 to 27.0.5 ([0e62a01](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/0e62a01c225360302fc52b0a6cb5f9cf8ec31ace))
* **deps:** bump fastify-autoload from 3.7.1 to 3.8.0 ([8db9786](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/8db9786ae54006f9c51f7afb3419232be1308643))
* **deps:** bump fastify-bearer-auth from 5.1.0 to 6.0.0 ([a5f8b05](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/a5f8b0527f2aafa914b206c29a7622b918bae93d))
* **deps:** bump under-pressure from 5.6.0 to 5.7.0 ([f6c7c86](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/f6c7c86edefed23e86f93b502d77e161c733ab7c))

### [3.0.5](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/compare/v3.0.4...v3.0.5) (2021-06-17)


### Dependencies

* **deps:** bump actions/upload-artifact from 2.2.3 to 2.2.4 ([609de13](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/609de13488e68c1cb58997c3f4a3667b935bd6fe))
* **deps:** bump fastify from 3.17.0 to 3.18.0 ([56a6c51](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/56a6c5195f98a55d6681e46a7ff5d4d25c334995))
* **deps:** bump fastify-disablecache from 2.0.1 to 2.0.2 ([895b01c](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/895b01c6e60333d9d7a00521cf01b9ff89c0241f))

### [3.0.4](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/compare/v3.0.3...v3.0.4) (2021-06-16)


### Bug Fixes

* **config:** `isProduction` and `prettyPrint` conditionals ([#174](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/issues/174)) ([4d33632](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/4d3363275ec147c568cc90d09d7be4fa0ef3f7a1))


### Continuous Integration

* fix key usage in `action/setup-node` ([71b56bb](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/71b56bb09e10f87795b5fb4fc5ef1cc66cc0b0ee))


### Miscellaneous

* **ci:** replace `node-version` key with shorter `node` ([#155](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/issues/155)) ([bead259](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/bead259d82ca657ea22daaeec956725fb9cc3a67))
* **dockerfile:** consolidate consecutive `run` instructions ([#157](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/issues/157)) ([3b6404d](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/3b6404dcc22cf5469804d1145e0121056742f0eb))
* **env:** remove pre-filled process load env values in template ([#159](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/issues/159)) ([a7f4306](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/a7f430608765fdfc56c161f7a07d2054d7c9db8a))
* **workflows:** remove `stale.yml` ([1683d5a](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/1683d5af62a66359c4eec4eb0af2e2591e1b200e))


### Documentation

* **readme:** grammar and wordiness fixes ([01e5def](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/01e5defcfd4dafd8d839cc995a5104894d72e84a))


### Dependencies

* **deps-dev:** bump @commitlint/cli from 12.1.1 to 12.1.4 ([a341e1a](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/a341e1ad0f66cbbae0600d4c3de0d94912f53f4a))
* **deps-dev:** bump @commitlint/config-conventional ([c672c7e](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/c672c7ec6303e1dd81da78c29b7e9f30715965b8))
* **deps-dev:** bump eslint from 7.26.0 to 7.27.0 ([42db51e](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/42db51ed16deee214fbcc96f7635008ff606e68a))
* **deps-dev:** bump eslint from 7.27.0 to 7.28.0 ([5d7323b](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/5d7323baea05c2b85bac2b21abe4d29758fe658e))
* **deps-dev:** bump eslint-plugin-import from 2.22.1 to 2.23.4 ([21b28e5](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/21b28e55d35db21441638315ba2798e0a4cf6f9a))
* **deps-dev:** bump eslint-plugin-jsdoc from 34.0.1 to 35.1.2 ([4517246](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/451724649abfe350e008e48d907cf571515d5985))
* **deps-dev:** bump eslint-plugin-jsdoc from 35.1.2 to 35.3.0 ([df932d6](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/df932d60fa13a33cb5b51def5590e181723fb9aa))
* **deps-dev:** bump jest from 26.6.3 to 27.0.3 ([09f9e2d](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/09f9e2d6de972e3417e17c1f49280e466193c43c))
* **deps-dev:** bump jest from 27.0.3 to 27.0.4 ([c0f6e07](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/c0f6e07ab647bc2b6d9d8c16e4c7cb03ea3fba28))
* **deps-dev:** bump pino-pretty from 4.8.0 to 5.0.1 ([98e1afc](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/98e1afc7e54de57d2b668ffac84d7316e2f799c9))
* **deps-dev:** bump pino-pretty from 5.0.1 to 5.0.2 ([b2dd076](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/b2dd076846e24d119fd090169e980539d72692eb))
* **deps-dev:** bump prettier from 2.3.0 to 2.3.1 ([70f404a](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/70f404a070d2dbb08823818464aaf0680fd49ba8))
* **deps:** bump actions/cache from 2.1.5 to 2.1.6 ([97c5d2b](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/97c5d2b218c7a7aba233cb27cb5314f8602d7b0f))
* **deps:** bump dotenv from 9.0.2 to 10.0.0 ([5e5aeef](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/5e5aeefa369198ac962be508c9a32c0f119f0295))
* **deps:** bump fastify from 3.15.1 to 3.17.0 ([af96241](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/af96241fc73c9e9c14ad21be7091ba4c319203b0))
* **deps:** bump fastify-disablecache from 2.0.0 to 2.0.1 ([0646aa4](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/0646aa49cfc5b207e280cf2d5d997f05fe696374))
* **deps:** bump glob-parent from 5.1.1 to 5.1.2 ([013be4b](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/013be4b6a7b0259b58a1c4b44ac05c48c55afd33))
* **deps:** bump normalize-url from 4.5.0 to 4.5.1 ([c3503cc](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/c3503cc360bf2ed14ffda95907f2772496112506))
* **deps:** bump wagoid/commitlint-github-action from 3.1.3 to 3.1.4 ([0f5bc0e](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/0f5bc0ee7bd7fbc00177661f72a7a610451c15d7))
* **deps:** bump ws from 7.4.2 to 7.4.6 ([907b0fa](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/907b0faa6df421b5b5c00ff35de2d4c0e3d91f06))

### [3.0.3](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/compare/v3.0.2...v3.0.3) (2021-05-12)


### Bug Fixes

* **config:** `LOG_LEVEL` env variable validation ([1d129b5](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/1d129b5a6e0ae9a63a8764ac850090b0cb3a29f8))


### Continuous Integration

* **link-check:** run once a week on monday ([30df7bc](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/30df7bc0e450e340d3a1ba92de12377ab264767a))


### Dependencies

* **deps-dev:** bump autocannon from 7.2.0 to 7.3.0 ([49ee450](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/49ee450baaff005ae7caf570b9e314d8807d482c))
* **deps-dev:** bump eslint from 7.25.0 to 7.26.0 ([47b8034](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/47b80349413576c16e5a69debdd98f411a7ce6c0))
* **deps-dev:** bump eslint-plugin-jsdoc from 33.1.0 to 34.0.1 ([ad881cb](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/ad881cbc0954c20308c4efcfe30b18e4c403ee8b))
* **deps-dev:** bump glob from 7.1.6 to 7.1.7 ([8dd74df](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/8dd74df8ace2a92c1d924867e3be4c4da1480d95))
* **deps-dev:** bump pino-pretty from 4.7.1 to 4.8.0 ([3565997](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/3565997ce81b0a8f8f305c1dcaa92fba657188a2))
* **deps-dev:** bump prettier from 2.2.1 to 2.3.0 ([#153](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/issues/153)) ([4b0e08e](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/4b0e08e5b5145ade5766ea0639f9ad9ac23593b0))
* **deps:** bump brpaz/hadolint-action from 1.4.0 to 1.5.0 ([deafc5a](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/deafc5a576502b1981d6f7e593e80ee0c308b469))
* **deps:** bump dotenv from 8.5.1 to 9.0.2 ([9833661](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/9833661a48f68a1fa36469e2f78c002e76918988))
* **deps:** bump fastify-cors from 6.0.0 to 6.0.1 ([601b2e9](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/601b2e9172b25276485f20ca007ce380516fbed7))
* **deps:** bump fastify-floc-off from 1.0.0 to 1.0.1 ([0cb5da7](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/0cb5da768d38c18007740eb9033e8905cb2f50bc))
* **deps:** bump fluent-json-schema from 2.0.4 to 3.0.0 ([a442d82](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/a442d82473902f4dd12c4722a13c21c0a3429115))
* **deps:** bump GoogleCloudPlatform/release-please-action ([3522d93](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/3522d93f288bf421ee370b086094b22e170aef38))
* **deps:** bump wagoid/commitlint-github-action from 3.1.1 to 3.1.3 ([dc8fd31](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/dc8fd31f6919b159cf5277ab93c39944025737d0))

### [3.0.2](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/compare/v3.0.1...v3.0.2) (2021-05-05)


### Bug Fixes

* **routes/redirect:** schema support for duplicate query string params ([895adec](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/895adec4b8c269200e80bbb032bef3b1062d400d))


### Continuous Integration

* add nodejs v16 to test matrix ([6188e28](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/6188e284997308d86ed07d8b4b02faac1a13f108))


### Dependencies

* **deps-dev:** bump eslint-plugin-jsdoc from 33.0.0 to 33.1.0 ([50ab49d](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/50ab49dc0f8f843681113803ef87951d02aff531))
* **deps:** bump dotenv from 8.2.0 to 8.5.1 ([87b83a5](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/87b83a5d1f11a8cc742af58d01af77756cff7819))
* **deps:** bump wagoid/commitlint-github-action from v3.1.0 to v3.1.1 ([d7d8fc3](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/d7d8fc3e6f4e010047f7b5dc49085d06117e6d2a))

### [3.0.1](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/compare/v3.0.0...v3.0.1) (2021-05-04)


### Dependencies

* **deps:** bump fastify from 3.15.0 to 3.15.1 ([bbbe23b](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/bbbe23b0aa319f7057a259b0c78a86ade6a48c09))
* **deps:** bump fastify-reply-from from 5.2.0 to 5.3.0 ([d9ccbfe](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/d9ccbfe06b707c101bd0593c343d52a259bbb5ce))
* **deps:** bump GoogleCloudPlatform/release-please-action ([fa9ebf3](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/fa9ebf3505a3d7af240b3d32da4f1d151269e3eb))


### Documentation

* **readme:** compress duplicate setup steps into a single section ([#133](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/issues/133)) ([e1a1df5](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/e1a1df59e9b93091f28d17a4ae346d7b637a76c9))

## [3.0.0](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/compare/v2.0.0...v3.0.0) (2021-04-30)


### ⚠ BREAKING CHANGES

* remove support for nodejs v10, as it is EOL as of 2021-04-30

### Features

* **config:** allow for rate and process limits to be user configured ([733842f](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/733842f03ff35341b7e6798188a41315b5008555))
* **server:** add process-load/503 handling ([580f551](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/580f55117ae39b4dbcb093065d04d55cbd82591d))
* **server:** add rate limiter ([8561b05](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/8561b05b617a5c9b2207231e0027e26f12bc1b9c))
* **server:** disable google floc support ([abb06c2](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/abb06c26c4b219de61ded1dc8b13554981349b1e))


### Bug Fixes

* **config:** plugin defaults ([0e07cfc](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/0e07cfc22c8f59be525e800d781edb02e62ccf2e))
* **config:** re-add removed defaults ([916362d](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/916362d7972ee27d6bfca811a9415137f6bb7d7f))
* **routes:** hide options routes from swagger docs ([373c1e5](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/373c1e56e6402f39f803d9221b646525530acc64))


### Continuous Integration

* do not run coveralls steps/jobs on forks ([f20cb68](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/f20cb681d6a387b844cdce51c398f1b0492163f2))
* **link-check:** fix skip regex ([698b82a](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/698b82af028c5f4f88649c6a5aef36f06a19de9e))
* **typoci:** add "pino" to excluded words ([6d0070c](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/6d0070c05f23da67bb7677519d340c3984f6c2ed))


### Documentation

* grammar and readability fixes ([be22b12](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/be22b1256e2d21078b1d4a87ad899854c5242558))


### Dependencies

* **deps-dev:** bump autocannon from 7.0.5 to 7.2.0 ([f41d0d8](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/f41d0d816412bb4acf21b164462209310ed1ce6f))
* **deps-dev:** bump eslint from 7.23.0 to 7.25.0 ([ddd9023](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/ddd9023af525cca582845ddbbf5e90b22cedd0ad))
* **deps-dev:** bump eslint-config-prettier from 8.1.0 to 8.3.0 ([96491fd](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/96491fd0d6c8091963e986f3b3ed2fb3b74136d2))
* **deps-dev:** bump eslint-plugin-jest from 24.3.4 to 24.3.6 ([7764a9c](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/7764a9c94726a33acba54e77e7194d6e5e60d06e))
* **deps-dev:** bump eslint-plugin-jsdoc from 32.3.0 to 33.0.0 ([37c3464](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/37c34640033dd9f4a491476737fb942a2b59aa98))
* **deps-dev:** bump eslint-plugin-promise from 4.3.1 to 5.1.0 ([2398403](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/2398403cde32230494ea00ac5ebb785e66a03b53))
* **deps-dev:** bump faker from 5.5.2 to 5.5.3 ([fad0359](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/fad0359ac1d0d2409c9208ecc3a681c11ef9e3d0))
* **deps:** bump actions/cache from v2.1.4 to v2.1.5 ([776ef72](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/776ef728b5816d6e8c4f1b31894d44cc7afd8684))
* **deps:** bump actions/github-script from v3.1.1 to v4.0.2 ([c6cb4a9](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/c6cb4a93f85bef08414dc28437d735ef52219091))
* **deps:** bump actions/upload-artifact from v2.2.2 to v2.2.3 ([be78b3e](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/be78b3ed91fbe25bf140bef07f3b5b42ba768349))
* **deps:** bump brpaz/hadolint-action from v1.3.1 to v1.4.0 ([a506acc](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/a506accef9b039b45c6024c143ed6f6fadefae26))
* **deps:** bump fastify from 3.14.1 to 3.15.0 ([b0d544e](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/b0d544e7127bdbd495c3b6d8c418293b332d4779))
* **deps:** bump fastify-auth from 1.0.1 to 1.1.0 ([4acac11](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/4acac11b6a40629bcb6e705851cba12a0abc2132))
* **deps:** bump fastify-autoload from 3.6.0 to 3.7.1 ([f67c09c](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/f67c09c44d78f54c664bbc419c684e86182d7607))
* **deps:** bump fastify-cors from 5.2.0 to 6.0.0 ([1dea750](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/1dea750b33af217df806ebee1db48d15059d7ad5))
* **deps:** bump fastify-disablecache from 1.0.6 to 2.0.0 ([01e71bf](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/01e71bfb2c855c309abae5027e39409c3de0ad8f))
* **deps:** bump GoogleCloudPlatform/release-please-action ([daaeadb](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/daaeadb30c303ba59556c65a800a2a944fc25010))
* **deps:** bump jose from 2.0.4 to 2.0.5 ([dd6a30a](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/dd6a30ace21e0860b3f7457d6f67f6af1b549a43))
* **deps:** bump jwks-rsa from 2.0.2 to 2.0.3 ([6453cd2](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/6453cd2ce37290629cec5aef9d2c604d77c974e5))
* **deps:** bump pino from 6.11.2 to 6.11.3 ([382f089](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/382f08935ab9ee2e6f1e6e73fcd6b86358e3420e))
* **deps:** bump typoci/spellcheck-action from v0.4.0 to v1.1.0 ([561ff42](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/561ff4227c63275dc8510daa708ca7c1399a6dc7))


### Miscellaneous

* **config:** remove redundant conditionals ([a6df6ee](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/a6df6ee748be0381b7d0fd475717fb415f8d6cb1))
* **env:** add whitespace ([1faa835](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/1faa835e21832f58419c067c88709a538215a70e))
* remove support for nodejs v10 ([e312626](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/e312626e8e068ae75ee55151728963ee85a912e9))

## [2.0.0](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/compare/v1.1.0...v2.0.0) (2021-04-06)


### ⚠ BREAKING CHANGES

* `CORS_METHODS` env variable removed

### Features

* add support for cors preflight requests ([413be7d](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/413be7d880dfed91745046cbc1adee69515c36b1))
* **config:** support `access-control-allow-credentials` cors header ([7ce463c](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/7ce463c4d650be20482effacaf12b11f05ba29de))


### Bug Fixes

* **config:** comma-delimited string support for cors origin value ([d65b6e3](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/d65b6e395db6f22eedd2aedafbbf22b91b1765f9))


### Miscellaneous

* **env.template:** add note discouraging reflecting cors origin ([fe5f70b](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/fe5f70bf2ee0d74fa4fde1465018d1e443d76a68))
* **env.template:** remove bad example ([9073804](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/907380429e9e622d45770ea7afde0935103d7ac8))
* **tests:** standardise test file names ([60d9810](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/60d98105656a7be868183e010fb39f092f35b124))


### Documentation

* **readme:** grammar fix ([1d2a59e](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/1d2a59ea8b9708ce8c4f55991eabdb2050241d4b))


### Continuous Integration

* add cleanup-run job ([6f33a77](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/6f33a774fdc98709e78e9099758b592f7595caca))


### Dependencies

* **deps-dev:** bump @commitlint/cli from 12.0.1 to 12.1.1 ([cc6c618](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/cc6c6183b6b0a3a2518b0af06b59b0b66b384ff3))
* **deps-dev:** bump @commitlint/config-conventional ([d58a95f](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/d58a95fef0486a36cc06331d3557fc0e2a4004b3))
* **deps-dev:** bump eslint-plugin-jest from 24.3.2 to 24.3.4 ([5f226fc](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/5f226fc731ac938bdffeede9565682f3a0f976fb))
* **deps-dev:** bump faker from 5.5.1 to 5.5.2 ([1a05d75](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/1a05d75171c271f735dab729147bffd6874e33c7))
* **deps:** bump actions/github-script from v3.1.0 to v3.1.1 ([5e89427](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/5e89427c123e72fc5a4895a614a234c20a2489d5))
* **deps:** bump fastify-reply-from from 5.1.0 to 5.2.0 ([52542f6](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/52542f6eee6c8b6f70233f36ce9389400f7fa35a))
* **deps:** bump wagoid/commitlint-github-action from v3.0.6 to v3.1.0 ([45a915f](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/45a915f8d434778fb076a134bb57368c927acc04))

## [1.1.0](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/compare/v1.0.4...v1.1.0) (2021-03-30)


### Features

* **server:** use `strict-origin-when-cross-origin` referrer policy ([87e3a94](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/87e3a94045bc29186b7fa51a69d976b6fee9fd10))


### Continuous Integration

* **automerge:** move automerge job into new workflow ([ebe70c2](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/ebe70c29ebf4a2660e8b9144cb1c01a29c0f1736))
* **ci:** ignore dependabot prs for commit message linting ([79b46ec](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/79b46ec33d0442183447c98cd71216d45e3cb132))
* **stale:** shorten workflow name ([9129758](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/9129758b92ee98122bc738b1de12d46463805f40))
* **workflows:** run only on push and pulls to master branch ([77c36c2](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/77c36c24e390bf7a0935cb708ad3b628a4108a37))


### Dependencies

* **deps-dev:** bump autocannon from 7.0.4 to 7.0.5 ([2219177](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/2219177baf1b88a19a4a1803e2dd044cc8b48378))
* **deps-dev:** bump eslint from 7.21.0 to 7.23.0 ([538040d](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/538040d0481c1e4f371b4cc0753d7ccbda480d96))
* **deps-dev:** bump eslint-plugin-jest from 24.1.5 to 24.3.2 ([fcc9397](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/fcc9397de111dd7109b9c7c9db8bce7a715abda4))
* **deps-dev:** bump eslint-plugin-jsdoc from 32.2.0 to 32.3.0 ([7b5ff2a](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/7b5ff2a28e1a78b3b2eec7471baa88c36c271b24))
* **deps-dev:** bump faker from 5.4.0 to 5.5.1 ([a123d57](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/a123d5734b236c972e124f450d4123e9cc2b70d3))
* **deps-dev:** bump husky from 4.3.8 to 6.0.0 ([d2b385b](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/d2b385b08de5f9f8ad93ce6c687e93fecf1c0ef6))
* **deps-dev:** bump pino-pretty from 4.5.0 to 4.7.1 ([1422942](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/14229420e88ca495bf5d33f53a79c57964745f14))
* **deps:** bump actions/stale from v3.0.17 to v3.0.18 ([2dfe966](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/2dfe966be4d831477feaf1c77b2b21f6d941ed5a))
* **deps:** bump fastify from 3.12.0 to 3.14.1 ([56019f1](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/56019f1035434aece2b7c45c288f8fbfa55b272d))
* **deps:** bump fastify-autoload from 3.5.2 to 3.6.0 ([8d71004](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/8d7100463b02d26aff8c1ae84d8996bd4d04b4dd))
* **deps:** bump fastify-disablecache from 1.0.4 to 1.0.6 ([6498da3](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/6498da3e8b4e4ec0d67413479b13d67518371f22))
* **deps:** bump fastify-helmet from 5.2.0 to 5.3.1 ([db66248](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/db66248506967b657995e14c5aa0032f27fe0259))
* **deps:** bump fastify-reply-from from 5.0.1 to 5.1.0 ([8a11f0e](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/8a11f0eaf57983f3ef0dfe24485f44772b9330db))
* **deps:** bump GoogleCloudPlatform/release-please-action ([b3a417f](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/b3a417f9e2c4fdcee58a67fedf52944b69fe0931))
* **deps:** bump jwks-rsa from 1.12.3 to 2.0.2 ([8c8f1b1](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/8c8f1b199e53017c1defa13e94152d3fd8dc0463))
* **deps:** bump pino from 6.11.1 to 6.11.2 ([791f26d](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/791f26d7a876c258c5253565081a11d841d363fd))
* **deps:** bump typoci/spellcheck-action from v0.3.0 to v0.4.0 ([509832c](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/509832c1f19c7904b7100fc77587a0e59ec78f41))
* **deps:** bump wagoid/commitlint-github-action from v3.0.1 to v3.0.6 ([c27ae7f](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/c27ae7f767edc76fa9f174a2dbc570353f6bb947))
* **docker:** remove now optional `version` value ([056dd01](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/056dd0110cbc18b483b71fca34820962687b0d19))


### Miscellaneous

* **config:** move `pino-pretty` config out of script ([7d5c119](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/7d5c11945e5352aef048650a96789d52fbb7b0f0))
* **env.template:** add default cors settings ([da1fa28](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/da1fa28e593add6d9ce87778798bb9ca0d46546a))
* **prettierignore:** add yarn lock file ([647f2b7](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/647f2b7d4317d9a4ce1123a9f730f5cf2d412337))
* **readme:** replace jpg ydh logo with svg ([45849fc](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/45849fcbe2c8e55f516b9f329530d6a66455a6f9))
* remove contraction usage in comments ([5141d5b](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/5141d5b1db6facb068b74271a0f1c7fb3b235c45))
* **workflows:** rename ci and perf sections ([e3e360f](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/e3e360f0fc9c23de5399d1fa0c5b1415ba5d9fae))

### [1.0.4](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/compare/v1.0.3...v1.0.4) (2021-03-03)


### Documentation

* **readme:** fix broken link ([6f153a6](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/6f153a623dd714032568acfc4c68404dcac54af7))
* **readme:** shorten links ([557ade3](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/557ade3fdc9401804d6177f8d7445cefac0833e9))


### Dependencies

* **dependabot:** set commit message prefix; lower pull limit ([bed8472](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/bed84722c8273d90168d98e8ba9718031c7ae439))
* **deps-dev:** bump @commitlint/cli from 11.0.0 to 12.0.1 ([#51](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/issues/51)) ([400cc86](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/400cc865af5800f883ef1e4a83d6c3b1faddbbfb))
* **deps-dev:** bump @commitlint/config-conventional ([81ba8f0](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/81ba8f01a55372c6e2c6653e22411ca041bcf127))
* **deps-dev:** bump autocannon from 7.0.3 to 7.0.4 ([#49](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/issues/49)) ([d63ccf2](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/d63ccf2176e31ece0879f39be846a97a8929a010))
* **deps-dev:** bump eslint from 7.20.0 to 7.21.0 ([#52](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/issues/52)) ([3c9512b](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/3c9512b29206cd70107a61797d34b7a41c894859))
* **deps-dev:** bump eslint-config-prettier from 7.2.0 to 8.1.0 ([d8ea7bc](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/d8ea7bcd20b9478e5f7f12e5af5af27ee777d766))
* **deps-dev:** bump eslint-plugin-jest from 24.1.3 to 24.1.5 ([f68d360](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/f68d3601ffbeb9e5bd89aef0351f04c78cdfca4c))
* **deps-dev:** bump eslint-plugin-jsdoc from 32.0.1 to 32.2.0 ([#44](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/issues/44)) ([61c9f7d](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/61c9f7d97c5ac73f17c949b83f3cf96cf512c8f2))
* **deps-dev:** bump lodash from 4.17.20 to 4.17.21 ([#47](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/issues/47)) ([a586984](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/a5869846246e8bf252cc2148ee0d670ca8196710))
* **deps:** bump fastify-autoload from 3.4.2 to 3.5.2 ([#45](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/issues/45)) ([a4ddd8a](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/a4ddd8a255eebfccca552c272baf2815b08699cb))
* **deps:** bump fastify-reply-from from 4.0.0 to 5.0.1 ([#53](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/issues/53)) ([6772801](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/6772801bd51af469862a1a61a852dbe7d9e775a5))
* **deps:** bump fluent-json-schema from 2.0.3 to 2.0.4 ([#50](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/issues/50)) ([046b46e](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/046b46e897ac912a0bfb5cf82e787bcb72ef362a))
* **deps:** bump jwks-rsa from 1.12.2 to 1.12.3 ([#46](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/issues/46)) ([f2acbef](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/f2acbef9311b15458e5858f2f176654e4682edec))
* **deps:** bump wagoid/commitlint-github-action from v2.2.3 to v3.0.1 ([d91449b](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/d91449bb3cbaa33555abf1e7af1948e65636195c))
* **deps:** specify minor and hotfix versions ([c85c252](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/c85c252468227cd7ec893cca29c30010eb7ab6f6))


### Miscellaneous

* add link check workflow ([e3c9653](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/e3c9653ae3dc398becdd57a746e93fa6524b0c70))
* automate release and changelog generation ([948c147](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/948c147a7a949690621e4471092607ca013d6ede))
* **codeql:** remove autobuild action ([26bdc5d](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/26bdc5dd6b1df52d0ebb7e9f731ca7675624bc42))
* **linkcheck:** extend ignored urls ([ec4347c](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/ec4347c16b83473ad89e9e7953b923bae57c3ee7))
* **lint-check:** compress patterns ([c18d9cb](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/c18d9cb9196ddd01d678e08ac606d235cc507450))
* **prettier:** create separate files to allow for CI/CD to use prettier config ([#55](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/issues/55)) ([7447f54](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/7447f5469e7677897cbb40c5ff0e9d5b10bff0e3))
* replace stalebot with github action ([5894e44](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/5894e4488c85b31d900ba650eccd9a54afb1f9c6))
* require `commit-lint` job to pass before automerge ([f36dbb6](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/f36dbb633165195cc6a5528bab8a7276de1836ce))
* **vscode:** remove conflicting prettier ext setting ([6f81c23](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/6f81c23778866d9692b143e7d6d35701135faff0))
* **workflows:** move release steps into `cd` workflow ([6ff81f9](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/6ff81f9dc1b276b0fd9df74b5a0da9a41b56bc37))
* **workflows:** remove redundant comments ([3604402](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/3604402f72ea571f7480f4023c73520136402134))
* **workflows:** rename spellcheck workflow ([468eda5](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/468eda554381ba7412fee2fdc172016842c4b06d))
* **workflows:** tidy node-version syntax ([341909a](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/commit/341909a3a1a8be83ca0b5dd21d66a073d2f8fbe9))

### [1.0.3](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/compare/v1.0.2...v1.0.3) (2021-02-16)

-   build(deps-dev): bump eslint from 7.19.0 to 7.20.0 (#35) ([41a0a02](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/41a0a02)), closes [#35](https://github.com/Fdawgs/ydh-fhir-authentication-service/issues/35)
-   build(deps-dev): bump eslint-plugin-jsdoc from 31.6.0 to 32.0.1 (#36) ([ba32bba](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/ba32bba)), closes [#36](https://github.com/Fdawgs/ydh-fhir-authentication-service/issues/36)
-   build(deps-dev): bump eslint-plugin-promise from 4.2.1 to 4.3.1 (#34) ([2cc028e](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/2cc028e)), closes [#34](https://github.com/Fdawgs/ydh-fhir-authentication-service/issues/34)
-   build(deps-dev): bump faker from 5.2.0 to 5.4.0 (#33) ([621164f](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/621164f)), closes [#33](https://github.com/Fdawgs/ydh-fhir-authentication-service/issues/33)
-   build(deps-dev): pin husky major version ([e3331ba](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/e3331ba))
-   build(deps): bump actions/cache from v2 to v2.1.4 (#29) ([dbbfb89](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/dbbfb89)), closes [#29](https://github.com/Fdawgs/ydh-fhir-authentication-service/issues/29)
-   build(deps): bump env-schema from 2.0.1 to 2.1.0 (#38) ([44be350](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/44be350)), closes [#38](https://github.com/Fdawgs/ydh-fhir-authentication-service/issues/38)
-   build(deps): bump fastify from 3.11.0 to 3.12.0 (#31) ([ae9d9e3](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/ae9d9e3)), closes [#31](https://github.com/Fdawgs/ydh-fhir-authentication-service/issues/31)
-   build(deps): bump fastify-bearer-auth from 5.0.2 to 5.1.0 (#32) ([a848b15](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/a848b15)), closes [#32](https://github.com/Fdawgs/ydh-fhir-authentication-service/issues/32)
-   build(deps): bump pino from 6.11.0 to 6.11.1 (#37) ([b213c6a](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/b213c6a)), closes [#37](https://github.com/Fdawgs/ydh-fhir-authentication-service/issues/37)
-   build(deps): bump wagoid/commitlint-github-action from v2.0.3 to v2.2.3 (#30) ([ff0e918](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/ff0e918)), closes [#30](https://github.com/Fdawgs/ydh-fhir-authentication-service/issues/30)
-   ci: add commit-lint job ([a67a263](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/a67a263))
-   ci: replace typo ci app with action ([2ee435a](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/2ee435a))
-   ci(dependabot): ignore husky updates ([cda323f](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/cda323f))
-   style: shorten husky pre-push script ([2159b2a](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/2159b2a))
-   style(readme): add linebreaks between badges ([bc4fffe](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/bc4fffe))
-   style(scripts): rename `jest-coverage` to `jest:coverage` ([aa92e38](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/aa92e38))
-   style(tests): use apa header style for describe name params ([54ba1f8](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/54ba1f8))
-   chore: add 0bsd and unlicense to list of allowed licenses ([9560a3e](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/9560a3e))
-   chore: add apache-2.0 to list of allowed licenses ([dd2bf28](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/dd2bf28))
-   chore: add commitlint husky `commit-msg` hook ([1cc0fee](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/1cc0fee))
-   chore: add documentation style link to pr template ([2d510c2](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/2d510c2))
-   chore(vscode): add `redhat.vscode-yaml` as recommended extension ([fa56f02](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/fa56f02))
-   chore(vscode): add `updateImportsOnFileMove` setting ([afcb796](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/afcb796))
-   chore(vscode): add workspace settings and extensions ([d94c92c](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/d94c92c))
-   docs(contributing): add documentation style ([d224628](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/d224628))
-   docs(readme): add ignore scripts arg ([9885444](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/9885444))

### [1.0.2](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/compare/v1.0.1...v1.0.2) (2021-02-02)

-   build(deps-dev): bump pino-pretty from 4.4.0 to 4.5.0 ([7feda97](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/7feda97))
-   refactor(config): update openapi docs from v2.\*.\* to v3.\*.\* ([b08fd73](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/b08fd73))
-   fix(config): stop rotatinglogstream flooding stdout ([c2bbbb2](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/c2bbbb2))

### [1.0.1](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/compare/v1.0.0...v1.0.1) (2021-02-01)

-   fix(docker): use node command over npm ([f69649a](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/f69649a))
-   fix(routes/redirect): id regex ([6452018](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/6452018))
-   build(deps-dev): bump eslint from 7.18.0 to 7.19.0 (#17) ([20d0d67](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/20d0d67)), closes [#17](https://github.com/Fdawgs/ydh-fhir-authentication-service/issues/17)
-   build(deps-dev): bump eslint-plugin-jsdoc from 31.4.0 to 31.6.0 (#16) ([8da82ee](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/8da82ee)), closes [#16](https://github.com/Fdawgs/ydh-fhir-authentication-service/issues/16)
-   build(deps-dev): bump pino-pretty from 4.3.0 to 4.4.0 (#18) ([714381b](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/714381b)), closes [#18](https://github.com/Fdawgs/ydh-fhir-authentication-service/issues/18)
-   build(deps): bump fastify-disablecache from 1.0.3 to 1.0.4 (#21) ([228a509](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/228a509)), closes [#21](https://github.com/Fdawgs/ydh-fhir-authentication-service/issues/21)
-   build(deps): bump fastify-helmet from 5.1.0 to 5.2.0 (#15) ([09d6028](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/09d6028)), closes [#15](https://github.com/Fdawgs/ydh-fhir-authentication-service/issues/15)
-   chore: check that direct dependencies use permissible licenses ([a5d53d9](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/a5d53d9))
-   chore(routes): specify operationid and produces openapi spec values ([81d24da](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/81d24da))
-   refactor(server): use new exposed CSP dir from `fastify-helmet` ([8a27d8a](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/8a27d8a))
-   docs(readme): remove superfluous text in pm2 install instructions ([7719463](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/7719463))
-   style: capitalise headings correctly ([afc65ed](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/afc65ed))
-   style(ci): capitalise jobs and job step names ([0df68fd](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/0df68fd))
-   style(readme): capitalise headings correctly ([a4c84a4](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/a4c84a4))
-   style(readme): prettier badge shape ([b913667](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/b913667))
-   style(test_resources): capitalise request names ([6d4e290](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/6d4e290))
-   style(test_resources): fix name of some requests ([02659ec](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/02659ec))

## [1.0.0](https://www.github.com/Fdawgs/ydh-fhir-authentication-service/compare/v0.0.1...v1.0.0) (2021-01-27)

-   style: fix spacing ([e0bc579](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/e0bc579))
-   chore: add insomnia example requests ([b381e0f](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/b381e0f))
-   chore: add pull request template ([fa411e2](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/fa411e2))
-   chore: tidy leftover console logs ([917283c](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/917283c))
-   docs: bump coc from v1.4.0 to v2.0.0 ([4d6294e](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/4d6294e))
-   docs(readme): add acknowledgements section ([b524f15](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/b524f15))
-   docs(readme): add description ([f69d175](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/f69d175))
-   docs(readme): remove env arg from pm2 deployment step ([7f32969](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/7f32969))
-   docs(readme): remove reference to docs route ([04a82e2](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/04a82e2))
-   feat(routes): return reply from redirected url ([420e53a](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/420e53a))
-   feat(routes/redirect): validate accept request header ([23ebb21](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/23ebb21))
-   build(deps-dev): add husky for git hook handling ([cb1931d](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/cb1931d))
-   build(deps-dev): bump eslint-plugin-jsdoc from 31.0.8 to 31.4.0 (#13) ([5cbbb54](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/5cbbb54)), closes [#13](https://github.com/Fdawgs/ydh-fhir-authentication-service/issues/13)
-   build(deps-dev): bump faker from 5.1.0 to 5.2.0 (#12) ([08429ab](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/08429ab)), closes [#12](https://github.com/Fdawgs/ydh-fhir-authentication-service/issues/12)
-   build(deps): add fastify-accepts ([f4cf7cd](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/f4cf7cd))
-   build(deps): bump fastify from 3.10.1 to 3.11.0 (#10) ([1703bbb](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/1703bbb)), closes [#10](https://github.com/Fdawgs/ydh-fhir-authentication-service/issues/10)
-   build(deps): bump fastify-autoload from 3.4.0 to 3.4.2 (#11) ([2438f2a](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/2438f2a)), closes [#11](https://github.com/Fdawgs/ydh-fhir-authentication-service/issues/11)
-   build(deps): replace axios with fastify-reply-from ([4637669](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/4637669))
-   test(plugins/jwt-jwks-auth): ignore coverage ([bb81dc0](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/bb81dc0))
-   test(server): add cors assertions ([affbbf1](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/affbbf1))
-   test(server): add server tests ([967830a](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/967830a))
-   test(server): set missing redirecturl config value ([1a28b5a](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/1a28b5a))
-   fix(config): add required properties ([af8369c](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/af8369c))
-   fix(routes/redirect): add required properties ([3eb08c6](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/3eb08c6))
-   refactor(cors): add cors handling ([ea4b172](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/ea4b172))
-   refactor(plugins): convert jwt-jwks util to fastify plugin ([7e954d1](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/7e954d1))
-   refactor(routes/redirect): separate read and search routes ([24e6cd7](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/24e6cd7))
-   ci: cache on `node-version` as well as `os` ([0019285](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/0019285))
-   ci: fix license checker step ([d52e3d5](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/d52e3d5))
-   ci(github-actions): set `flag-name` for parallel coverage tests ([48c5f78](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/48c5f78))
-   ci(github-actions): set semver for coverallsapp ([9536bf0](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/9536bf0))

### 0.0.1 (2021-01-22)

-   feat(config): add jwt and bearer token env variable validation ([447aa69](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/447aa69))
-   feat(routes): add basic wildcard route ([0e004db](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/0e004db))
-   feat(routes): add healthcheck route ([f9d9b4c](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/f9d9b4c))
-   style: adhere to prettier standard ([5920fa4](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/5920fa4))
-   build(docker): speed up install by using `npm ci` over `npm install` ([6ba7b54](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/6ba7b54))
-   refactor(pm2): use repo name for instances; remove redundant env setting ([18671ae](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/18671ae))
-   test(config): remove failing assertion ([6d0d976](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/6d0d976))
-   chore: add template files ([635aabd](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/635aabd))
-   Initial commit ([1a2e995](https://github.com/Fdawgs/ydh-fhir-authentication-service/commit/1a2e995))
